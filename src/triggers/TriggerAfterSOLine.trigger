trigger TriggerAfterSOLine on PBSI__PBSI_Sales_Order_Line__c (after insert, after update, after delete) {
    Set<Id> listSaleOrderId = new Set<Id>();
    Set<id> allItemids = new Set<id>();
    if(!trigger.isdelete)
    {
        for( PBSI__PBSI_Sales_Order_Line__c newItem: trigger.new)
        {
            listSaleOrderId.add(newItem.PBSI__Sales_Order__c);
            allItemids.add(newItem.PBSI__Item__c);
        }
    }
    else if(trigger.isdelete)
    {
        for( PBSI__PBSI_Sales_Order_Line__c newItem: trigger.old)
        {           
            allItemids.add(newItem.PBSI__Item__c);
        }
    }
    List<PBSI__PBSI_Sales_Order__c> listSalesOrder = [select id,Name,PBSI__Status__c 
                                                      from PBSI__PBSI_Sales_Order__c 
                                                     where id in:listSaleOrderId];
    List<PBSI__PBSI_Sales_Order_Line__c> listSaleOrderLine = [select id,name,PBSI__Sales_Order__c,PBSI__Item__r.Name,PBSI__Quantity_Needed__c,PBSI__Quantity_Picked__c 
                                                              from PBSI__PBSI_Sales_Order_Line__c 
                                                              where PBSI__Sales_Order__c in:listSaleOrderId];
    for( PBSI__PBSI_Sales_Order__c item:listSalesOrder  )
    {
        boolean flag = true;
        for(PBSI__PBSI_Sales_Order_Line__c itemLine: listSaleOrderLine )
        {
            if(itemLine.PBSI__Sales_Order__c == item.Id )
            {
                
                if( itemLine.PBSI__Quantity_Needed__c != itemLine.PBSI__Quantity_Picked__c )
                {
                    //system.debug('itemLine:' + itemLine.PBSI__Quantity_Needed__c + '    ' + itemLine.PBSI__Quantity_Picked__c);
                    flag = false;
                    break;
                }
            }
        }
        if( flag == true)
        {
            //system.debug('itemLine: update to 5' );
            item.PBSI__Status__c = 'Production Complete';
            
        }
        else
        {
            if( item.PBSI__Status__c == 'Production Complete')
            {
                //system.debug('itemLine: update to 4' );
                item.PBSI__Status__c = 'Partially Complete';
                item.PBSI__Stage__c = 'Partially Packed';
            }
                
        }
    }
    
    if(allItemids.size() > 0)
    {
        Map<id,Decimal> ItemCount = new Map<id,Decimal>();
        List<PBSI__PBSI_Sales_Order_Line__c> soLine = [select id,PBSI__Item__c, PBSI__Quantity_Left_To_Ship__c 
                                                        from  PBSI__PBSI_Sales_Order_Line__c
                                                        where PBSI__Stage_Status__c ='Open' 
                                                        and (PBSI__Lot__r.Name = 'DocData' OR PBSI__Lot__r.Name = 'Shipped from DocData')
                                                        and PBSI__Item__c in:allItemids];
        for(PBSI__PBSI_Sales_Order_Line__c line: soLine)
        {
            Decimal val = line.PBSI__Quantity_Left_To_Ship__c == null? 0:line.PBSI__Quantity_Left_To_Ship__c;
            if(ItemCount.get(line.PBSI__Item__c) != null)
            {               
                Decimal count = ItemCount.get(line.PBSI__Item__c);
                count = count + val;
                ItemCount.put(line.PBSI__Item__c,count);
            }
            else
            {
                ItemCount.put(line.PBSI__Item__c,val);
            }   
        }
        List<PBSI__PBSI_Item__c> itemMaster = [select id, Allocated_at_DocData__c from PBSI__PBSI_Item__c where id in :allItemids];
        for(PBSI__PBSI_Item__c item :itemMaster)
        {
            item.Allocated_at_DocData__c = ItemCount.get(item.id);
        }
        update itemMaster;
    }
    
    TriggerUtils.IsAllowUpdateDeleteSO = true;
    update listSalesOrder;
}