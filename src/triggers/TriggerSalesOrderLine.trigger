trigger TriggerSalesOrderLine on PBSI__PBSI_Sales_Order_Line__c (before insert, before update) {
    
    if (trigger.isinsert || trigger.isupdate) {
        set<Id> soIds = new set<id>();
        for (PBSI__PBSI_Sales_Order_Line__c newItem : trigger.new) {
            if (newItem.PBSI__Sales_Order__c != null) {
                soIds.add(newItem.PBSI__Sales_Order__c);
            }
        }
        List<PBSI__PBSI_Sales_Order__c> listSalesOrder = [SELECT Id, Name,PBSI__Opportunity__c,PBSI__Opportunity__r.VAT_Code__c, Fulfilment__c
                                                          FROM PBSI__PBSI_Sales_Order__c WHERE Id IN : soIds ];
                
        Map<String, String> listTaxCodeName = new Map<String,String>();
        for( integer i = 0; i< listSalesOrder.size(); i++) {
            if( listSalesOrder[i].PBSI__Opportunity__c !=  null && String.isNotEmpty(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c)  ) {
                if( !listTaxCodeName.containsKey(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c.trim()) ) {
                    listTaxCodeName.put(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c.trim(),listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c.trim() );
                }   
            }
        }
        List<c2g__codaTaxCode__c> listcoda = [SELECT id,Name FROM c2g__codaTaxCode__c 
                                              WHERE name in: listTaxCodeName.values() ];
        
        Map<String,c2g__codaTaxCode__c> MapTwoCode = new Map<String,c2g__codaTaxCode__c>();
        
        for(integer i = 0; i< listSalesOrder.size();i++ ) {
            if( listSalesOrder[i].PBSI__Opportunity__c !=  null && String.isNotEmpty(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c)  ) {
                for( integer j = 0; j < listcoda.size();j++ ) {
                    if(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c.trim()== listcoda[j].Name.trim() )
                        MapTwoCode.put(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c.trim(), listcoda[j]);   
                }
            }
        }
        
        for ( PBSI__PBSI_Sales_Order_Line__c newItem : trigger.new ) {
            if ( newItem.PBSI__Sales_Order__c != null  ) {
                for (integer i = 0; i< listSalesOrder.size();i++ ) {
                    if ( newItem.PBSI__Sales_Order__c == listSalesOrder[i].id) {
                        if ( listSalesOrder[i].PBSI__Opportunity__c !=  null && String.isNotEmpty(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c)  ) {
                            c2g__codaTaxCode__c temp = MapTwoCode.get(String.valueOf(listSalesOrder[i].PBSI__Opportunity__r.VAT_Code__c).trim());    
                            if ( temp!= null ) {
                                newItem.Ascent2FF__Tax_Code_1__c = temp.id;
                            }
                        }
                        break;
                    }
                }                
            }            
        }
        
        /**Update Lot for fullfilment sales order**/
        Set<Id> fulfillmentSOIds = new Set<Id>();
        for (PBSI__PBSI_Sales_Order__c so : listSalesOrder) {
            if (so.Fulfilment__c == 'Fulfillment') {
                fulfillmentSOIds.add(so.Id);
            }
        }
        List<PBSI__PBSI_Sales_Order_Line__c> fulfillmentSOLines = new List<PBSI__PBSI_Sales_Order_Line__c>();
        for (PBSI__PBSI_Sales_Order_Line__c soLine : trigger.new) {
            if (fulfillmentSOIds.contains(soLine.PBSI__Sales_Order__c)) {
                fulfillmentSOLines.add(soLine);
            }
        }
        system.debug('#fulfillmentSOLines#' + fulfillmentSOLines);
        TriggerUtiSO.UpdateLotForFulfillmentSalesOrderLines(fulfillmentSOLines, false);
        /** END Update Lot for fullfilment sales order**/
    }
    
}