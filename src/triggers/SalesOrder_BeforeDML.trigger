trigger SalesOrder_BeforeDML on PBSI__PBSI_Sales_Order__c (before insert, after insert) {
    if(Trigger.IsInsert) {
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        if(Trigger.IsBefore){
            // PopulaterDataFromOpp(Trigger.NewMap); // ? before insert so ID is nul -> how NewMap is used here? Lack of responsibility
            PopulaterDataFromOpp(Trigger.New);
           // ChangeOwnerToLockDown(Trigger.New);
        } else if(Trigger.IsAfter) {
            // create shipping sales order line
            createSalesOrderLine(Trigger.NewMap.keySet());
        }
    }
     
   // private void ChangeOwnerToLockDown(List<PBSI__PBSI_Sales_Order__c> soList) {
    //    
    //    for (PBSI__PBSI_Sales_Order__c salesOrder : soList) {
    //        salesOrder.OwnerID = '00520000003HP96AAG';
     //   }
     //   //update soList;
    //}
    private Date calculateDispatchDate(string leadTime){
        if(leadTime == '1 - 2 Weeks'){
            return system.today().addDays(14);
        }else if (leadTime == '2 - 3 Weeks'){
            return system.today().addDays(21);
        }else if (leadTime == '3 - 4 Weeks'){
            return system.today().addDays(28);
        }else if (leadTime == '4 - 6 Weeks'){
            return system.today().addDays(42);
        }else if (leadTime == '6 - 8 Weeks'){
            return system.today().addDays(56);
        }else{
            return system.today().addDays(56);
        }
    }
    private void createSalesOrderLine(Set<Id> soIdSet) {
        
        // loop through soList
        List<PBSI__PBSI_Sales_Order__c> soList = [
            SELECT Id,PBSI__Opportunity__r.Shipping_and_Handling__c,PBSI__Opportunity__r.VAT_Code__c,PBSI__Opportunity__r.CurrencyIsoCode
            FROM PBSI__PBSI_Sales_Order__c
            WHERE Id IN :soIdSet
        ];
        Set<String> codaTaxCodeSet = new Set<String>();
        for(PBSI__PBSI_Sales_Order__c so: soList) {
            codaTaxCodeSet.add(so.PBSI__Opportunity__r.VAT_Code__c);
        }
        List<c2g__codaTaxCode__c> codaTaxList = [
            SELECT Id,Name
            FROM c2g__codaTaxCode__c
            WHERE Name IN :codaTaxCodeSet
        ];
        Map<String, c2g__codaTaxCode__c> codaTaxMap = new Map<String, c2g__codaTaxCode__c>();
        for(c2g__codaTaxCode__c c: codaTaxList) {
            codaTaxMap.put(c.Name, c);
        }
        // for each so, create 1 associated so line which has: price = opportunity.Shipping_and_Handling__c
        // ---------- quantity = 1, Tax Code 1 set as same as others so lines
        List<PBSI__PBSI_Sales_Order_Line__c> soLinesToBeInsert = new List<PBSI__PBSI_Sales_Order_Line__c>();
        // how to set tax code:
        // from opportunity.VAT Code -> this is tax code name
        // select IDX from c2g__codaTaxCode__c where name = VAT Code above
        // set IDX to so line.tax code 1
        // select 1 item where item group is misc, name contain shipping
        
        
        //TC Add
        set<id> oppIds = new set<id>();
        for(PBSI__PBSI_Sales_Order__c orderitem: soList)
        {
            if(!oppIds.contains(orderitem.PBSI__Opportunity__c))
            {
                oppIds.add(orderitem.PBSI__Opportunity__c);
            }
        }       
        List<opportunitylineitem> oppLine = [SELECT Id,
                                             Name,
                                             ProductCode ,
                                             ListPrice,
                                             OpportunityId,
                                             Quantity,
                                             UnitPrice,
                                             product2.Ascent4Prods__Item__c
                                             FROM OpportunityLineItem
                                             where OpportunityId IN:oppIds];   
        
        List<PBSI__PBSI_Sales_Order_Line__c> soLine = new List<PBSI__PBSI_Sales_Order_Line__c>();
        
        for(PBSI__PBSI_Sales_Order__c oItem:soList)
        {
            for(opportunitylineitem oppLineItem:oppLine)
            {
                if(oppLineItem.OpportunityId == oItem.PBSI__Opportunity__c && oppLineItem.product2.Ascent4Prods__Item__c != null)
                {
                    soLinesToBeInsert.add(new PBSI__PBSI_Sales_Order_Line__c
                                          (
                                              PBSI__Quantity_Needed__c = oppLineItem.Quantity,
                                              PBSI__Price__c = oppLineItem.UnitPrice,
                                              PBSI__Sales_Order__c = oItem.id,
                                              PBSI__Item__c = oppLineItem.product2.Ascent4Prods__Item__c
                                          ));
                }
            }
        }
        
        
        List<PBSI__PBSI_Item__c> shippingList = [SELECT Id FROM PBSI__PBSI_Item__c WHERE Name='SH000' AND PBSI__Item_Group__r.Name = 'MISC'];
        
        if(shippingList.size() > 0) {
            PBSI__PBSI_Item__c shippingItem = shippingList[0];
                
                for(PBSI__PBSI_Sales_Order__c so: soList) {
                    PBSI__PBSI_Sales_Order_Line__c tempSoLine = new PBSI__PBSI_Sales_Order_Line__c(
                        CurrencyIsoCode = so.PBSI__Opportunity__r.CurrencyIsoCode,
                        PBSI__Quantity_Needed__c = 1,
                        PBSI__Sales_Order__c = so.Id,
                        PBSI__Price__c = so.PBSI__Opportunity__r.Shipping_and_Handling__c,
                        PBSI__Item__c = shippingItem.Id
                    );
                    if(codaTaxMap.containsKey(so.PBSI__Opportunity__r.VAT_Code__c)) {
                        tempSoLine.Ascent2FF__Tax_Code_1__c = codaTaxMap.get(so.PBSI__Opportunity__r.VAT_Code__c).Id;
                    }
                    soLinesToBeInsert.add(tempSoLine);
                }
        }
        
        insert soLinesToBeInsert;
        //End TC Add
        
    }
    
    // private void PopulaterDataFromOpp(Map<ID, PBSI__PBSI_Sales_Order__c> soList){
    private void PopulaterDataFromOpp(List<PBSI__PBSI_Sales_Order__c> soList) {
        Set<Id> oppIds = new Set<ID>();
        //for(PBSI__PBSI_Sales_Order__c so:soList.values()){
        for( PBSI__PBSI_Sales_Order__c so:soList ){
            oppIds.add(so.PBSI__Opportunity__c);
        }
        
        Map<Id, Opportunity> map_id_Opp = new Map<Id, Opportunity>([Select Id, Estimated_Dispatch_Date__c, Shipper__c, 
                                                                    Wired_For__c, Packing__c,
                                                                    Packing_Shipping_Requirements__c,Delivery_Type__c,
                                                                    Shipping_Street_N__c,Customer_Order_Number__c, 
                                                                    Required_For__c, Description, Bespoke__c,City__c,
                                                                    Shipping_Country_N__c,State_County__c,Shipping_Postcode_N__c,
                                                                    Urgent__c, Call_Email_Before_Dispatch__c, Sooner_Delivery_If_Possible__c,
                                                                    Samples__c, Allow_Return__c, Combine_with_Order__c,
                                                                    Dispatch_Method__c,Lead_Time__c,
                                                                    Delivery_Notes_For_Courier__c
                                                                    FROM Opportunity WHERE ID IN :oppIds]);
        // for(PBSI__PBSI_Sales_Order__c so:soList.values()){
        for (PBSI__PBSI_Sales_Order__c so:soList ) {
            Opportunity opp = map_id_Opp.get(so.PBSI__Opportunity__c);
            if (opp != null) {
                so.Dispatch_Date__c = calculateDispatchDate(opp.Lead_Time__c);//opp.Estimated_Dispatch_Date__c;
                so.Shipper__c = opp.Shipper__c;
                so.Customer_Order_Number__c = opp.Customer_Order_Number__c;
                so.Description__c = opp.Description;
                so.Bespoke__c = opp.Bespoke__c;
                so.Urgent__c = opp.Urgent__c;
                so.Call_Email_Before_Dispatch__c = opp.Call_Email_Before_Dispatch__c;
                so.Sooner_Delivery_If_Possible__c = opp.Sooner_Delivery_If_Possible__c;
                so.Samples__c = opp.Samples__c;
                so.Allow_Return__c = opp.Allow_Return__c;
                so.Combine_with_Order__c = opp.Combine_with_Order__c;
                so.Dispatch_Method__c = opp.Dispatch_Method__c;
                so.Locked__c = true;
                so.PBSI__Delivery_Streetnew__c = opp.Shipping_Street_N__c;                
                so.PBSI__Delivery_City__c = opp.City__c;
                so.PBSI__Delivery_Country__c = opp.Shipping_Country_N__c;
                so.PBSI__Delivery_County__c = opp.State_County__c;
                so.PBSI__Delivery_Postal_Code__c = opp.Shipping_Postcode_N__c;
                
                // Create new / check existing corresponding fields from the ‘Supply Chain Management’ 
                // section on Production org with Sandbox. Replicate where required. 
                // Create Formula fields for these on Sales Order to pull values from Opportunity (Vietnam)
                so.Packing_Shipping_Requirements__c = opp.Packing_Shipping_Requirements__c;                
                so.Wired_For__c = opp.Wired_For__c;
                so.Packing__c = opp.Packing__c;
                so.Delivery_Type__c = opp.Delivery_Type__c;
                so.Lead_Time__c = opp.Lead_Time__c;
                so.Estimated_Dispatch_Date__c = calculateDispatchDate(opp.Lead_Time__c);
                so.Required_For__c = opp.Required_For__c;
                so.Delivery_Notes_For_Courier__c = opp.Delivery_Notes_For_Courier__c;
            }
        }
    }
 }