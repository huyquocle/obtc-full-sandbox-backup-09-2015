trigger beforeInsertFFSalesInvoice on c2g__codaInvoice__c (before insert) {

    Map<Id, Id> sin2soMap = new Map<Id, Id>();
    Map<Id, PBSI__PBSI_Sales_Order__c> soMap = new Map<Id, PBSI__PBSI_Sales_Order__c>();
    
        // c2g__Opportunity__c
        // Ascent2FF__Sales_Order__c
        // PBSI__Opportunity__c

    for (c2g__codaInvoice__c si : Trigger.new) {
        //system.debug('***** si.Ascent2FF__Sales_Order__c: ' + si.Ascent2FF__Sales_Order__c);
        
        //friends remind friends to bulkify
        sin2soMap.put(si.Id, si.Ascent2FF__Sales_Order__c);
    }

    List<PBSI__PBSI_Sales_Order__c> soList = [SELECT Id, Name, PBSI__Opportunity__c FROM PBSI__PBSI_Sales_Order__c WHERE Id in: sin2soMap.values()];
    
    //system.debug('***** soList: ' + soList);

    for (PBSI__PBSI_Sales_Order__c so : soList) {
        soMap.put(so.Id, so);
    }

    for (c2g__codaInvoice__c sin : Trigger.new) {
        //friends remind friends to bulkify
        String soId = sin2soMap.get(sin.Id);
        //system.debug('***** soId: ' + soId);        
        
        if (soId != null){
            system.debug('***** opp: ' + soMap.get(soId).PBSI__Opportunity__c);
            sin.c2g__Opportunity__c = soMap.get(soId).PBSI__Opportunity__c;
        }
    }
}