trigger SendMovementItemToDocData on PBSI__Movement_Line__c (after insert,before insert) {
    if(trigger.isbefore && trigger.isInsert)
    {
        if(TriggerUtils.movementLineAjustmentReason !='' )
        {
            for(PBSI__Movement_Line__c item:trigger.new)
            {
                item.PBSI__Reason_Code__c = TriggerUtils.movementLineAjustmentReason;
            }
        }
    }
    else if(trigger.isinsert && trigger.isAfter)
    {
        List<DocdataLocationName__c> docLocation = [select Location_Name__c from DocdataLocationName__c];
        if(docLocation != null && docLocation.size() > 0)
        {
            String defaultName = docLocation[0].Location_Name__c;
            List<PBSI__Movement_Line__c> newItems = [select id,Name,PBSI__Transfer_Quantity__c,
                                                     PBSI__Notestext__c,
                                                     PBSI__To_Location__r.Name,
                                                     PBSI__Item__r.Name,
                                                     PBSI__Item__c
                                                     FROM PBSI__Movement_Line__c 
                                                     where id in:trigger.newmap.keyset() 
                                                     AND PBSI__MJType__c = 'Transfer Posting Location to Location' ];
            List<postStockTransfer> transferList = new List<postStockTransfer>();
            List<Stock_Delivery_History__c> sdhList = new List<Stock_Delivery_History__c>();
            PostStockTransfer mlItem;
            Stock_Delivery_History__c sdhItem;
            DateTime currentDate = system.now();
            for(PBSI__Movement_Line__c item:newItems )
            {
                //in case of quantity = 0, will not transfer to docdata
                if(item.PBSI__Transfer_Quantity__c > 0) {
                    if(item.PBSI__To_Location__r.Name == defaultName )
                    {
                       mlItem = new  PostStockTransfer();
                       mlitem.PO = string.valueof(currentDate.format('dd/MM/yyyy'));
                       mlitem.sku = item.PBSI__Item__r.Name;
                       mlitem.Quantity = integer.valueof( item.PBSI__Transfer_Quantity__c);
                       mlitem.DateOfDelivery = string.valueof(currentDate);
                       mlitem.Comment = (item.PBSI__Notestext__c == null ? '' : string.valueof(item.PBSI__Notestext__c).abbreviate(254));
                       transferList.add(mlitem);
                       
                       sdhItem = new Stock_Delivery_History__c();                   
                       sdhItem.Date_of_Transfer__c = currentDate;
                       sdhItem.Item__c = item.PBSI__Item__c;
                       sdhItem.Transfer_Quantity__c = item.PBSI__Transfer_Quantity__c;
                       sdhItem.SKU__c = item.name;
                       sdhItem.Comment__c = item.PBSI__Notestext__c;
                       sdhItem.Stock_Delivery_Status__c = 'In warehouse';
                       sdhItem.Po__c = string.valueof(currentDate.format('dd/MM/yyyy'));
                       sdhList.add(sdhItem);             
                    }
                }
                
            }           
            if(transferList.size() > 0)
            {
                if(sdhList.size() > 0)
                {
                    insert sdhList;
                }
                Set<id> newIds = new set<id>();
                for(Stock_Delivery_History__c item:sdhList)
                {
                    newIds.add(item.id);
                }   
                ODIWebServices.postStockTransfer(JSON.serialize(transferList),newIds);                             
            }
        }        
    }
}