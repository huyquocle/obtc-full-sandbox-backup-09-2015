trigger Trigger_SalesOrderAfterUpdate on PBSI__PBSI_Sales_Order__c ( after insert ,before update, after update) {
    if (Trigger.IsBefore){
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        for (PBSI__PBSI_Sales_Order__c newItem: trigger.new) {            
            PBSI__PBSI_Sales_Order__c oldItem = trigger.oldMap.get(newItem.id);
            if (newItem.PBSI__Stage__c == 'Packed' && oldItem.PBSI__Stage__c != 'Packed') {
                if(newItem.Hold_Until__c == null)
                {
                    if (!newItem.Payment_Required__c) {
                        newItem.PBSI__Stage__c = 'Ready for Dispatch';
                    }
                }else if(newItem.Hold_Until__c <= System.Today()){          
                    if (!newItem.Payment_Required__c) {
                        newItem.PBSI__Stage__c = 'Ready for Dispatch';
                    }
                }
            }         
        }
    }
    if( Trigger.isAfter)
    {
        if( trigger.isInsert)
        {
            List<EmailTemplate> emailtemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where Id ='00X20000001qF7X' limit 1];
            for( PBSI__PBSI_Sales_Order__c item : Trigger.new)
            {
                if( item.PBSI__Due_Date__c != null && item.Suppress_automated_customer_notification__c == false
                  && item.PBSI__Status__c == 'Approved for Production' && item.Website_Order__c == true )
                {
                    //system.debug('send email and log');
                    string bodyText = string.valueof(emailtemplate.get(0).Body);
                    insertActiviryTask( item, bodyText );
                }
            }
        }
        if( trigger.isUpdate)
        {
            Map<Id, PBSI__PBSI_Sales_Order__c> mapOld = new Map<Id, PBSI__PBSI_Sales_Order__c>();
            for( PBSI__PBSI_Sales_Order__c item : Trigger.old )
            {
                if( !mapOld.containsKey(item.id))
                {
                    mapOld.put(item.id, item);
                }
            }
            List<EmailTemplate> emailtemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where Id ='00X20000001qF7X' limit 1];
            
            
            
            
            system.debug('debug:activity history');
            for( PBSI__PBSI_Sales_Order__c item : Trigger.new)
            {
                
                if( item.PBSI__Due_Date__c != null && item.Suppress_automated_customer_notification__c == false
                  && item.PBSI__Status__c == 'Approved for Production' && item.Website_Order__c == true )
                {
                    PBSI__PBSI_Sales_Order__c itemOld = mapOld.get(item.Id);
                    if(!( itemOld.PBSI__Due_Date__c != null && itemOld.Suppress_automated_customer_notification__c == false
                        && itemOld.PBSI__Status__c == 'Approved for Production' && itemOld.Website_Order__c == true ))
                    {
                        // do some thing
                        system.debug('debug:activity history:activity');
                        string bodyText = string.valueof(emailtemplate.get(0).Body);
                        insertActiviryTask( item, bodyText );
                       
                    }
                }
            }
        }
    }
    
   /* if (Trigger.IsAfter) {
        //Create Invoice for SO
        List<PBSI__PBSI_Sales_Order__c> lstSO = new list<PBSI__PBSI_Sales_Order__c>();
        
        for (PBSI__PBSI_Sales_Order__c newItem: trigger.new) {
            PBSI__PBSI_Sales_Order__c oldItem = trigger.oldMap.get(newItem.id);
            
            if (newItem.PBSI__Stage__c == 'Ready for Dispatch' && newItem.PBSI__Stage__c != oldItem.PBSI__Stage__c){
                lstSO.add(newItem);
            }
        }
        
        //if (lstSO.size() > 0) {
        //    TriggerUtiSO.CreateFFInvoice(lstSO);
       // }
    }*/
    public void insertActiviryTask(PBSI__PBSI_Sales_Order__c item, string bodyTextTemp )
{
    String bodyText  = bodyTextTemp ;
     Task soTask = new Task();
        soTask.Subject = 'Sales Order ' + item.Name+ ' is approved';
        soTask.ActivityDate = item.PBSI__Due_Date__c;
        soTask.Priority = 'Nomarl';
        Id userId = UserInfo.getUserId();
        soTask.OwnerId  = userId;
        soTask.Status = 'Completed';
        soTask.WhoId = item.PBSI__Contact__c;
        soTask.WhatId = item.id;
    	system.debug('#bodyText:' + bodyText);
        bodyText = bodyText.replace('{!PBSI__PBSI_Sales_Order__c.PBSI__Customer__c}',item.CustomSoName__c);
        bodyText = bodyText.replace('{!PBSI__PBSI_Sales_Order__c.Name}',item.Name);
        /*List<string> listday = new List<String>{string.valueof(item.PBSI__Due_Date__c.day()),
                                                string.valueof(item.PBSI__Due_Date__c.month()),
                                                string.valueof(item.PBSI__Due_Date__c.year())};
    
        string Due_Date = String.format('{0}/{1}/{2}}', string item.PBSI__Due_Date__c );
        */
        
        bodyText = bodyText.replace('{!PBSI__PBSI_Sales_Order__c.PBSI__Due_Date__c}',string.valueof( item.PBSI__Due_Date__c));
        if( item.Description__c != null)
            bodyText = bodyText.replace('{!PBSI__PBSI_Sales_Order__c.PBSI__Comments__c}', item.Description__c);
        else
            bodyText = bodyText.replace('{!PBSI__PBSI_Sales_Order__c.PBSI__Comments__c}', '');
         
        soTask.Description = bodyText;//string.format( bodyText);
        
        
        insert soTask;
    	
    	Task oppTask = new Task();
    	oppTask.Subject = 'Sales Order ' + item.Name+ ' is approved';
        oppTask.ActivityDate = item.PBSI__Due_Date__c;
        oppTask.Priority = 'Nomarl';
        //Id userId = UserInfo.getUserId();
        oppTask.OwnerId  = userId;
        oppTask.Status = 'Completed';
        oppTask.Description = bodyText;
    	oppTask.WhatId = item.PBSI__Opportunity__c;
    	insert oppTask;
    
}
}