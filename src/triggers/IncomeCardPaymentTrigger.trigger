/**
 *  @author: Paul Harvie (Income Systems)
 *  @date: 2014-09-12  
 *  @description: IncomeCard Payment Trigger
	  
 */



trigger IncomeCardPaymentTrigger on Income_Card_Payment__c (after insert) {
	
	system.debug('####IncomeCardPaymentTrigger fir');
	set<ID> OpportunityIDs = new set<ID>();
	
	Opportunity[] oppstoupdate = new Opportunity[]{};
	
	
	for(Income_Card_Payment__c i : Trigger.new){
		system.debug('####Opportunity id ' + i.Opportunity__c);
		OpportunityIDs.add(i.Opportunity__c);
    }
                
      
    Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>([select id, Stagename, Total_Amount_Taken__c,  Company_Name__c, AccountID, Web_Site_Order__c from Opportunity where Id In :OpportunityIDs ]);
    system.debug('############### oppMap =' + oppMap);
    
    
    
    set<Opportunity> oppSet = new Set<Opportunity>([select id, Stagename, Total_Amount_Taken__c,  Company_Name__c, AccountID from Opportunity where Id In :OpportunityIDs ]);
      
    
    set<ID> AccountIDs = new set<ID>();
    for(Opportunity thisOpp : oppSet)
    {
    	AccountIDs.add(thisOpp.accountid);
    }
    system.debug('############### AccountIDs =' + AccountIDs);
    
    
    Map<ID, Account> accMap = new Map<ID, Account>([Select Id, Discount_Percent__c, Discount_Lookup__c,	Do_Not_Display_discount__c  From Account where Id In :AccountIDs ]);
    system.debug('############### AccountIDs =' + AccountIDs);
    
    for (Income_Card_Payment__c thiscc :System.Trigger.new) 
    {
    
      	Boolean hasDescription = false;
      	
      	Boolean hasDiscount = false;
      	Boolean hasAdditionalDiscount = false;
      	
    	Opportunity thisopp = oppMap.get(thiscc.Opportunity__c);
    	system.debug('############### thisopp =' + thisopp);
    	
    	Account thisAcc = accMap.get(thisopp.AccountId);
    	system.debug('############### thisAcc =' + thisAcc);
    
        Boolean donotdisplaydiscount = thisAcc.Do_Not_Display_discount__c;   
    
        OpportunityLineItem[] theseItems = [Select PricebookEntry.Product2Id, PricebookEntry.Product2.Name, PricebookEntry.ProductCode, 
	    UnitPrice, TotalPrice,  Quantity,  Description, CurrencyIsoCode, Additional_Discount__c, Discount, Unit_Selling_Price__c, ListPrice  From OpportunityLineItem  where OpportunityId = :thisOpp.Id];
	   	    
	    system.debug('## theseItems ' + theseItems );
	    
	    for(OpportunityLineItem thisitem : theseItems)
	    {
	    	if (!String.isBlank(thisitem.Description) ) hasDescription = true;
	    	
	    	decimal accdiscount = (thisAcc.Discount_Percent__c != null ?  thisAcc.Discount_Percent__c : 0);
			decimal itemdiscount = (thisitem.Additional_Discount__c != null ?  thisitem.Additional_Discount__c : 0);
			
			if (itemdiscount > 0 )  hasAdditionalDiscount = true;
			
			if (accdiscount > 0) hasDiscount = true;
			
	    }
	    system.debug('## hasDescription ' + hasDescription );
	    system.debug('## hasDiscount ' + hasDiscount );
	    system.debug('## hasAdditionalDiscount ' + hasAdditionalDiscount );
	    
        string producttablehtml = '';
        /*
        producttablehtml = '<table border="1" cellspacing="1" cellpadding="1">';
        producttablehtml += '<th><b>Id</b></th>';
		producttablehtml += '<th><b>Product Code</b></th>';
		producttablehtml += '<th><b>Description</b></th>';
		producttablehtml += '<th><b>Quantity</b></th>';
		producttablehtml += '<th><b>Currency</b></th>';
		producttablehtml += '<th><b>Net Amount</b></th>';
		producttablehtml += '</tr>';
		*/
        
        producttablehtml = '<table style="width:100%;" cellpadding="0">';
        //producttablehtml += '<th class=\'tableheader\' style="border-bottom:0px solid #000;"><b>Id</b></th>';
		
		//if (thisopp.Company_Name__c == 'Original BTC France')
		//{
		//	producttablehtml += '<tr>';
		//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Nom du produit</b></th>';
		//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Code</b></th>';
		//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Prix HT</b></th>';
		//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Qtt</b></th>';
		//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Total HT</b></th>';
		//	producttablehtml += '</tr>';
		//}
		//else 
		//{
			
			//if (thisopp.Web_Site_Order__c)
			//{
			//	producttablehtml += '<tr>';
				//producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Product Name</b></th>';
			//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Code</b></th>';
			//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Description</b></th>';
			//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Net Price</b></th>';
			//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Qty</b></th>';
			//	producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Subtotal</b></th>';
			//	producttablehtml += '</tr>';
			//}
			//else
			//{
			
			producttablehtml += '<tr>';
			producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Code</b></th>';
			producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Description</b></th>';
			if (hasDescription)
			{			
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France')  producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Description du Secteur</b></th>';
				else producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Line Description</b></th>';
			}
			
			
			if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France')  producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Qtt</b></th>';
			else producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Qty</b></th>';
	
		
			
			
			//producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:left;"><b>Code</b></th>';
		
			if (!donotdisplaydiscount)
			{
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France')  producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Prix HT</b></th>';
				else producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>List Price</b></th>';
	
				
				//producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Price</b></th>';
			
				//producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Account Discount</b></th>';
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France') producttablehtml += ((hasDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Remise de Compte</b></th>' : '');
				else producttablehtml += ((hasDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Account Discount</b></th>' : '');
				
				//producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Discount</b></th>';
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France') producttablehtml += ((hasAdditionalDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Remise supplémentaire</b></th>' : '');
				else producttablehtml += ((hasAdditionalDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Additional Discount</b></th>' : '');
			
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France') producttablehtml += ((hasDiscount || hasAdditionalDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Prix ​​de vent</b></th>' : '');
			    else producttablehtml += ((hasDiscount || hasAdditionalDiscount) ? '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Selling Price</b></th>' : '');
	
			
			}
			else
			{
				if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France') producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Prix ​​de vent</b></th>';
			    else producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Selling Price</b></th>';
	
			}
			
				
			
			if (thisopp.Company_Name__c == 'Original BTC France' || thisopp.Company_Name__c == 'LM France') producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Total HT</b></th>';
			else producttablehtml += '<th class=\'tableheader\' style="border-bottom:1px solid #000;text-align:right;"><b>Total</b></th>';
			producttablehtml += '</tr>';
			//}
		//}
		
		integer i = 1;
		String CartCurrencySymbol;
		for( OpportunityLineItem thisItem : theseItems)
		{
		
			if(thisItem.CurrencyIsoCode == 'EUR')
			{
				CartCurrencySymbol = '€';
			}
			else if(thisItem.CurrencyIsoCode == 'GBP')
			{
				CartCurrencySymbol = '£';
			}
			else
			{
				CartCurrencySymbol = '£';
			}
			 
			 
			
			
			
			//if (thisopp.Web_Site_Order__c)
			//{ 
			//	producttablehtml += '<tr>';
			//	producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.PricebookEntry.ProductCode + '</td>';
				//producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.Description + '</td>';
			//	producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.PricebookEntry.Product2.Name + '</td>';
			//	producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' +CartCurrencySymbol+ thisItem.UnitPrice + '</td>';
			//	integer quant = (integer)thisItem.Quantity; 
				
			//	producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + quant + '</td>';
			//	producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' +CartCurrencySymbol+ thisItem.TotalPrice + '</td>'; 
				//decimal unitprice =  thisItem.UnitPrice + 0.001;
				//system.debug('############### thisItem.UnitPrice =' + thisItem.UnitPrice);
				//system.debug('############### unitprice =' + unitprice);
			//	producttablehtml += '</tr>';
			//}
			//else
			//{
			producttablehtml += '<tr>';
			producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.PricebookEntry.ProductCode + '</td>';
			// no product name
			producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.PricebookEntry.Product2.Name + '</td>';
			
			if (hasDescription) 
			{
				//producttablehtml += '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.Description + '</td>';
				producttablehtml += ((thisItem.Description != null)  ? '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.Description + '</td>' : '<td style="padding-left:5px;text-align:left;" class="pagetext"></td>');
			}
			
			//producttablehtml += ((hasDescription == true && thisItem.Description != null)  ? '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.Description + '</td>' : '<td style="padding-left:5px;text-align:left;" class="pagetext"></td>');
			//producttablehtml += ((thisItem.Description != null)  ? '<td style="padding-left:5px;text-align:left;" class="pagetext">' + thisItem.Description + '</td>' : '<td style="padding-left:5px;text-align:left;" class="pagetext"></td>');
			
			integer quant = (integer)thisItem.Quantity; 
			
			producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + quant + '</td>';
			
			//producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' +CartCurrencySymbol+ thisItem.UnitPrice + '</td>';
			
			if (!donotdisplaydiscount)
			{
				
				producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' +CartCurrencySymbol+ thisItem.ListPrice + '</td>';
				//producttablehtml +=  (hasDiscount) ? '<td style="padding-left:5px;text-align:right;" class="pagetext">' + CartCurrencySymbol+ thisItem.UnitPrice + '</td>' : '';
				
				decimal accdiscount = (thisAcc.Discount_Percent__c != null ?  thisAcc.Discount_Percent__c : 0);
				decimal itemdiscount = (thisItem.Additional_Discount__c != null ?  thisItem.Additional_Discount__c : 0);
				string accdiscountstring = accdiscount + '%';
				string itemdiscountstring = itemdiscount + '%';
				
				//producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + accdiscountstring + '</td>';
				producttablehtml += ((hasDiscount  ) ? '<td style="padding-left:5px;text-align:right;" class="pagetext">' + accdiscountstring + '</td>' : '');
				
				//producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + itemdiscountstring + '</td>';
				producttablehtml += ((hasAdditionalDiscount ) ? '<td style="padding-left:5px;text-align:right;" class="pagetext">' + itemdiscountstring + '</td>' : '');
				
				
				producttablehtml += ((hasDiscount || hasAdditionalDiscount) ? '<td style="padding-left:5px;text-align:right;" class="pagetext">' + CartCurrencySymbol+ thisItem.Unit_Selling_Price__c + '</td>' : ''); 
				
			}
			else
			{
				producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + CartCurrencySymbol+ thisItem.Unit_Selling_Price__c + '</td>'; 
			}
			
			//producttablehtml += ((hasDiscount || hasAdditionalDiscount) ? '<td style="padding-left:5px;text-align:right;" class="pagetext">' + CartCurrencySymbol+ thisItem.Unit_Selling_Price__c + '</td>' : ''); 
	
			producttablehtml += '<td style="padding-left:5px;text-align:right;" class="pagetext">' + CartCurrencySymbol+ thisItem.TotalPrice + '</td>'; 
			//decimal unitprice =  thisItem.UnitPrice + 0.001;
			//system.debug('############### thisItem.UnitPrice =' + thisItem.UnitPrice);
			//system.debug('############### unitprice =' + unitprice);
			producttablehtml += '</tr>';
			//}
			
			
			i++;
			
			
		}
		producttablehtml += '</table>';
      
        system.debug('############### producttablehtml =' + producttablehtml);
        
        thisopp.Shopping_Cart_HTML__c = producttablehtml;
      	system.debug('############### update this opp =' + thisopp);
	    oppstoupdate.add(thisopp);
      
    
        /*
    
          ShoppingCartHtml = "<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\">";
                    ShoppingCartHtml += "<th><b>Id</b></th>";
                    ShoppingCartHtml += "<th><b>Product Code</b></th>";
                    ShoppingCartHtml += "<th><b>Description</b></th>";
                    ShoppingCartHtml += "<th><b>Quantity</b></th>";
                    ShoppingCartHtml += "<th><b>Currency</b></th>";
                    ShoppingCartHtml += "<th><b>Net Amount</b></th>";
                    ShoppingCartHtml += "</tr>";
    
    
    
                        WebSiteCartHtml += "<tr>";

                        WebSiteCartHtml += "<td>" + dr["Description"].ToString() + "</td>";
                        WebSiteCartHtml += "<td>" + dr["ProductCode"].ToString() + "</td>";
                        WebSiteCartHtml += "<td>" + CartCurrencySymbol + lineunitprice.ToString("0.00") + "</td>";
                        WebSiteCartHtml += "<td>" + dr["Quantity"].ToString() + "</td>";
                        WebSiteCartHtml += "<td style='float:right;'>" + CartCurrencySymbol + lineprice.ToString("0.00") + "</td>";
                        WebSiteCartHtml += "</tr>";
    	
    	
    	*/
    }
    
    if (oppstoupdate != null) 
	{
		update oppstoupdate;
	}
    
    


}