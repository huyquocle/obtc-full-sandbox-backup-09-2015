trigger MovementJournalTriggers on PBSI__Movement_Journal__c (before insert, before update) {
	TriggerUtils.IsAllowUpdateDeleteSO = true;
}