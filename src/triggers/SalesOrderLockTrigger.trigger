trigger SalesOrderLockTrigger on PBSI__PBSI_Sales_Order__c (before update, before delete) {
    if( TriggerUtils.IsAllowUpdateDeleteSO )
    {
        return;
    }
    Id userId = UserInfo.getUserId();
    Id ProfileId = UserInfo.getProfileId();
    //system.debug('profileId:' + ProfileId);
    //system.debug('profileId1:' + string.valueOf(ProfileId));
    //00e20000001GGmsAAG
    //00e20000001GGmsAAG
    
    if( String.valueOf(userId) == '00520000003idPhAAI' || String.valueOf(userId) == '00520000003HP96AAG' || String.valueOf(userId) == '00520000003iFfnAAE' || string.valueOf(ProfileId) == '00e20000001GGmsAAG' )
    {
        return;
    }
    
    for(PBSI__PBSI_Sales_Order__c item: Trigger.New ){
        if(item.Locked__c){
            item.addError('The Sales Order is locked and can only be updated by Hettie, Ben or Nikki. If you need to make changes, please request assistance from Hettie, Ben or Nikki.');}
        else{ 
            item.Locked__c = true;
            TriggerUtils.IsAllowUpdateDeleteSO = true;
        } 
    }
    
}