trigger CreateSO on Opportunity (before update,after update) {
    if(trigger.isAfter)
    {
        List<PBSI__PBSI_Sales_Order__c> orderList = new List<PBSI__PBSI_Sales_Order__c>();
        Map<id,Opportunity> holdChanged = new Map<id,Opportunity>();
        for(Opportunity newItem:trigger.new)
        {
        	system.debug(newItem.Estimated_Dispatch_Date__c);
            if(trigger.oldMap.get(newItem.id).StageName!='Closed Won' 
               && newItem.StageName=='Closed Won'
               && newItem.Company_Name__c != 'Original BTC France'
               && newItem.Company_Name__c != 'LM France')
            {
                orderList.add(new PBSI__PBSI_Sales_Order__c(Locked__c=true,
                    PBSI__Opportunity__c = newItem.id,
                    Hold_Until__c = newItem.Hold_Until__c,
                    Payment_Required__c = newItem.Payment_Required_Status__c,
                    PBSI__Customer__c =newItem.accountid ));            
            }
            if(newItem.Hold_Until__c !=null
               && trigger.oldMap.get(newItem.id).Hold_Until__c != newItem.Hold_Until__c)
            {
                holdChanged.put(newItem.id,newItem);
            }
        }
        if(orderList.size() > 0 && !TriggerUtils.IsInsertSO) // Prevent it to run insert again.
        { 
            TriggerUtils.IsInsertSO = true;
            TriggerUtils.IsAllowUpdateDeleteSO = true;
            insert orderList;
            Map<id,Opportunity> newopp = new Map<id,Opportunity>([select id,PBSI__Sales_Order__c 
                                                                  from Opportunity 
                                                                  where id in:trigger.newmap.keyset()]);
            //Update Created SO id back to Opp
            for(PBSI__PBSI_Sales_Order__c insertedItem: orderList)
            {            
                newopp.get(insertedItem.PBSI__Opportunity__c).PBSI__Sales_Order__c = insertedItem.id;
                newopp.get(insertedItem.PBSI__Opportunity__c).PBSI__hasSO__c = '1';
            }
            update newopp.values();
        }
        if(holdChanged.size() > 0 && !TriggerUtils.isAfterOpportunityRunOnceCreateSO)
        {
            List<PBSI__PBSI_Sales_Order__c> orderUpdateList =
                [select id,Hold_Until__c,PBSI__Opportunity__c 
                 FROM PBSI__PBSI_Sales_Order__c where PBSI__Opportunity__c IN:holdChanged.keySet()];
            
            if(orderUpdateList != null && orderUpdateList.size() > 0)
            {
                for(PBSI__PBSI_Sales_Order__c oritem:orderUpdateList)
                {
                    oritem.Hold_Until__c = holdChanged.get(oritem.PBSI__Opportunity__c).Hold_Until__c;
                }
                TriggerUtils.isAfterOpportunityRunOnceCreateSO = true;
                update orderUpdateList;
            }
        }
    }
}