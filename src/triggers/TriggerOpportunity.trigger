trigger TriggerOpportunity on Opportunity (before update, after update) {
    
    if (trigger.isBefore && !TriggerUtils.isBeforeOpportunityRunOnce) {
        TriggerUtils.isBeforeOpportunityRunOnce = true;
        Map<String, String> listTaxCodeName = new Map<String,String>();
        for( Opportunity Newitem : trigger.new )
        {
            if( Newitem.StageName == 'Closed Won' || Newitem.PBSI__Sales_Order__c != null   )
            {
                if(  String.isNotEmpty(Newitem.VAT_Code__c))
                    listTaxCodeName.put(Newitem.VAT_Code__c.trim(),Newitem.VAT_Code__c.trim());
            }
        }
        List<c2g__codaTaxCode__c> listcoda = [select id,Name from c2g__codaTaxCode__c 
                                              where name in: listTaxCodeName.values() ];
        for( Opportunity Newitem : trigger.new )
        {
            if( Newitem.StageName == 'Closed Won' || Newitem.PBSI__Sales_Order__c != null )
            {
                boolean flag = false;
                if(  String.isEmpty(Newitem.VAT_Code__c)&& Newitem.StageName == 'Closed Won')
                    Newitem.addError('The VAT Code on Opportunity does not match any FinancialForce VAT Codes. Please request assistance from a Salesforce Administrator.');
                
                if(  String.isEmpty(Newitem.VAT_Code__c))
                    continue;
                
                for(c2g__codaTaxCode__c item: listcoda )
                {
                    if(Newitem.VAT_Code__c.trim() == item.Name.trim()  )
                        flag = true;
                }
                if (flag == false)
                    Newitem.addError('The VAT Code on Opportunity does not match any FinancialForce VAT Codes. Please request assistance from a Salesforce Administrator.');
            }
        }
    }
    
    if (trigger.isAfter && !TriggerUtils.isAfterOpportunityRunOnce){
        TriggerUtils.isAfterOpportunityRunOnce = true;
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        set<id> listOppId = trigger.newMap.keySet();
        List<PBSI__PBSI_Sales_Order__c> listSO = [select Id,PBSI__Opportunity__c,
                                                  Call_Email_Before_Dispatch__c,
                                                  Samples__c,
                                                  Allow_Return__c,
                                                  PBSI__Delivery_Company__c,
                                                  PBSI__Delivery_Streetnew__c,
                                                  PBSI__Delivery_City__c,
                                                  PBSI__Delivery_State__c,
                                                  PBSI__Delivery_Postal_Code__c,
                                                  PBSI__Delivery_Country__c,
                                                  Delivery_Notes_For_Courier__c,
                                                  Packing_Shipping_Requirements__c,
                                                  Customer_Order_Number__c
                                                  from PBSI__PBSI_Sales_Order__c 
                                                  WHERE PBSI__Opportunity__c in:listOppId];
        
        Map<Id,PBSI__PBSI_Sales_Order__c> listSOTemp = new Map<Id,PBSI__PBSI_Sales_Order__c>();
        
        Map<Id, Opportunity> mapPaymentRequiredOpp = new Map<Id, Opportunity>();
        for (Opportunity opp : trigger.New) {
            if( opp.StageName == 'Closed Won')
            {
                for( PBSI__PBSI_Sales_Order__c so : listSO)
                {
                    if( so.PBSI__Opportunity__c == opp.Id)
                    {
                        if(!listSOTemp.containsKey(so.id) )
                        {
                            so.Call_Email_Before_Dispatch__c = opp.Call_Email_Before_Dispatch__c;
                            so.Samples__c = opp.Samples__c;
                            so.Allow_Return__c = opp.Allow_Return__c;
                            so.PBSI__Delivery_Company__c = opp.Shipping_Company__c;
                            so.PBSI__Delivery_Streetnew__c  = opp.Shipping_Street_N__c;
                            so.PBSI__Delivery_City__c = opp.City__c;
                            so.PBSI__Delivery_State__c = opp.State_County__c;
                            so.PBSI__Delivery_Postal_Code__c = opp.Shipping_Postcode_N__c;
                            so.PBSI__Delivery_Country__c = opp.Shipping_Country_N__c;
                            so.Delivery_Notes_For_Courier__c = opp.Delivery_Notes_For_Courier__c;
                            so.Packing_Shipping_Requirements__c = opp.Packing_Shipping_Requirements__c;
                            so.Customer_Order_Number__c = opp.Customer_Order_Number__c;
                            listSOTemp.put(so.id,so);
                        }
                    }
                }
            }
            
            //update payment required of related sales orders
            // Opportunity oldOpp = trigger.oldMap.get(opp.id);
            //if (opp.Payment_Required__c != oldOpp.Payment_Required__c) {
            mapPaymentRequiredOpp.put(opp.Id, opp);
            //}
        }
        
        if (mapPaymentRequiredOpp.size() > 0) {
            List<PBSI__PBSI_Sales_Order__c> paymentRequiredSoList = [SELECT ID, Payment_Required__c, PBSI__Opportunity__c 
                                                                     FROM PBSI__PBSI_Sales_Order__c
                                                                     WHERE PBSI__Opportunity__c IN :trigger.new];
            if (paymentRequiredSoList.size() > 0) {
                for (PBSI__PBSI_Sales_Order__c so :paymentRequiredSoList) {
                    Opportunity opp = mapPaymentRequiredOpp.get(so.PBSI__Opportunity__c);
                    so.Payment_Required__c = opp.Payment_Required_Status__c;
                }
                update paymentRequiredSoList;
            }
        }
        
        if (listSOTemp.size() > 0) {
            TriggerUtils.IsAllowUpdateDeleteSO = true;
            update listSOTemp.values();
        }
        
        /*Get Fullfilment Sales Order List for updating Lot*/
        Set<Id> oppIds = new Set<Id>();
        for (Opportunity opp : Trigger.NewMap.values()) {
            if (opp.Fulfilment__c == 'Fulfillment') {
                Opportunity oldOpp = trigger.oldMap.get(opp.id);
                if (oldOpp.Fulfilment__c != 'Fulfillment') {
                    oppIds.add(opp.Id);
                }
            }
        }
        List<PBSI__PBSI_Sales_Order__c> fulfillmentSalesOrders = [SELECT Id, Name,
                                                                  (SELECT id, PBSI__Item__c, PBSI__Item__r.Name, PBSI__Lot__c
                                                                   FROM PBSI__Sales_Order_Lines__r 
                                                                   WHERE PBSI__Item__r.Name !='SH000')
                                                                  FROM PBSI__PBSI_Sales_Order__c 
                                                                  WHERE PBSI__Opportunity__c IN : oppIds];
        List<PBSI__PBSI_Sales_Order_Line__c> soLines = new List<PBSI__PBSI_Sales_Order_Line__c>();
        for (PBSI__PBSI_Sales_Order__c so : fulfillmentSalesOrders) {
            for (PBSI__PBSI_Sales_Order_Line__c line : so.PBSI__Sales_Order_Lines__r) {
                soLines.add(line);
            }
        }
        
        TriggerUtiSO.UpdateLotForFulfillmentSalesOrderLines(soLines, true);
    }
    
}