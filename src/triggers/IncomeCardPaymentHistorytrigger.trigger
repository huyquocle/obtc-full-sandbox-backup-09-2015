/**
 *  @author: Paul Harvie (Income Systems)
 *  @date: 2014-09-12  
 *  @description: IncomeCard Payment History Trigger
	  
 */

trigger IncomeCardPaymentHistorytrigger on Income_Card_Payment_History__c (after insert) {
	set<ID> CardPaymentIDs = new set<ID>();
	set<ID> OpportunityIDs = new set<ID>();
	Opportunity[] sendEmailList = new Opportunity[]{};
	Opportunity[] oppstoupdate = new Opportunity[]{};
	
	for(Income_Card_Payment_History__c thisPaymentHistory : Trigger.new)
	{
		// no authorised payment
		//if(thisPaymentHistory.Payment_Status__c == 'Authorised')
		//{
			system.debug('####thisPaymentHistory.Income_Card_Payment__c ' + thisPaymentHistory.Income_Card_Payment__c);
			CardPaymentIDs.add(thisPaymentHistory.Income_Card_Payment__c);
		//}
	}
	
	
	
	for(Income_Card_Payment__c i : [select id,Opportunity__c from Income_Card_Payment__c where Id in :CardPaymentIDs ]){
		system.debug('####Opportunity id ' + i.Opportunity__c);
		OpportunityIDs.add(i.Opportunity__c);
    }
    
    
     Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>([select id, Stagename, Total_Amount_Taken__c, Billing_Contact__c,Company_Name__c,Web_Site_Order__c from Opportunity where Id In :OpportunityIDs ]);
	 
	 system.debug('############### oppMap =' + oppMap);	
    
     Map<ID,Income_Card_Payment__c> ccMap = new Map<ID,Income_Card_Payment__c>([select id, Opportunity__c, Card_Number_Ending__c,Card_Type__c  from Income_Card_Payment__c where id In :CardPaymentIDs ]);
	   
	 system.debug('############### ccMap =' + ccMap);	
	   
  	
  			
    
    

	for (Income_Card_Payment_History__c thiscchistory :System.Trigger.new) 
    {
    	system.debug('############### thiscchistory =' + thiscchistory);
    	
    	Income_Card_Payment__c thiscc = ccMap.get(thiscchistory.Income_Card_Payment__c);
		system.debug('############### thiscc =' + thiscc);
    	
    	Opportunity thisopp = oppMap.get(thiscc.Opportunity__c);
    	system.debug('############### thisopp =' + thisopp);
    	
    	decimal existingtotalamount = 0;
    	
    	if (thisopp.Total_Amount_Taken__c == null) existingtotalamount = 0;
    	else existingtotalamount = thisopp.Total_Amount_Taken__c;
    	
    	
    	if (thiscchistory.Transaction_Type__c == 'Sale' && thiscchistory.Payment_Status__c == 'Authorised' )
    	{
    		system.debug('############### this is an authorsed sale');
    		existingtotalamount += thiscchistory.Amount__c;
    		
    		if (thisopp.StageName != 'Closed Won') 
    		{
    			thisopp.StageName = 'Closed Won';
    			
    			//'Original BTC'
    			
    			//ID emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('DefaultPaymentConfirmationTemplateID').Value__c);
    			//if (thisopp.Web_Site_Order__c)
    			//{
	    		//	if (thisopp.Company_Name__c == 'Original BTC') emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLightingMatters').Value__c);
	    		//	if (thisopp.Company_Name__c == 'Original BTC France') emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLMFrance').Value__c);
	    		//	if (thisopp.Company_Name__c == 'Lighting Library') emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLL').Value__c);
	    		//}
    			
    			//System.debug('++++++++++++ emailtemplateid =' +  emailtemplateid);
    			
    			thisopp.Most_Recent_Payment_Amount__c = thiscchistory.Amount__c;
    			thisopp.Most_Recent_Payment_Card_Number__c = thiscc.Card_Number_Ending__c;
    			thisopp.Most_Recent_Payment_Card_Type__c = thiscc.Card_Type__c;
    			
    			sendEmailList.add(thisOpp);
    			//EmailUtils.sendEmail(emailtemplateid, thisOpp.Id, thisOpp.Shipping_Contact__c, orgwideemailaddressid );
    			
    		}
    	}
    	else if (thiscchistory.Transaction_Type__c == 'Refund' && thiscchistory.Payment_Status__c == 'Authorised' )
    	{
    		system.debug('############### this is an authorsed refund');
    		existingtotalamount -= thiscchistory.Amount__c;
    	}
    	else if (thiscchistory.Transaction_Type__c == 'Void' && thiscchistory.Payment_Status__c == 'Authorised' )
    	{
    		system.debug('############### this is an authorsed Void');
    		existingtotalamount = 0;
    	}
    	
    	//Select i.Transaction_Type__c, i.Payment_Status__c, i.Id, i.Amount__c From Income_Card_Payment_History__c i
	    thisopp.Total_Amount_Taken__c = existingtotalamount;
    	
    	system.debug('############### update this opp =' + thisopp);
	    oppstoupdate.add(thisopp);
	    
	    
		    
    	
    }

    
	if (oppstoupdate != null) 
	{
		update oppstoupdate;
	}
	
	for(Opportunity opp : sendEmailList)
	{
		
		ID orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('Defaultorgwideemailaddressid').Value__c);
  	 	System.debug('++++++++++++ orgwideemailaddressid =' +  orgwideemailaddressid);
		
		
		ID emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('DefaultPaymentConfirmationTemplateID').Value__c);
		System.debug('++++++++++++ opp.Company_Name__c =' +  opp.Company_Name__c);
		System.debug('++++++++++++ opp.Web_Site_Order__c =' +  opp.Web_Site_Order__c);
		//if (opp.Web_Site_Order__c)
		//{
			//System.debug('++++++++++++ opp.Company_Name__c =' +  opp.Company_Name__c);
			if (opp.Company_Name__c == 'Original BTC') 
			{
				emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationOriginalBTC').Value__c);
				orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressOriginalBTC').Value__c);
			}
			if (opp.Company_Name__c == 'Original BTC France') 
			{
				emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationOriginalBTCFrance').Value__c);
				orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressBTCFrance').Value__c);
			}
			if (opp.Company_Name__c == 'The Lighting Library')
			{
				 emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLL').Value__c);
				 orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressLL').Value__c);
			}
			if (opp.Company_Name__c == 'LM England') 
			{
				emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLMEngland').Value__c);
				orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressLMEngland').Value__c);
			}
			if (opp.Company_Name__c == 'LM France') 
			{
				emailtemplateid = (ID)string.valueOf(Application_Config__c.getInstance('PaymentConfirmationLMFrance').Value__c);
				orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressLMFrance').Value__c);
			}
		//}
		System.debug('++++++++++++ emailtemplateid =' +  emailtemplateid);
		try
		{
			EmailUtils.sendEmail(emailtemplateid, opp.Id, opp.Billing_Contact__c, orgwideemailaddressid );
		}
		catch(Exception err){}
	}
	

    //for(Opportunity i : [select id,StageName from Opportunity where Id in :OpportunityIDs]){
	//	system.debug('####Opportunity id ' + i.Id);
	//	i.StageName = 'Closed Won';
	//	system.debug('####Updating Opportunity');
	//	update i;
	//}
   
}