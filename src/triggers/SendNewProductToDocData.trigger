trigger SendNewProductToDocData on PBSI__PBSI_Item__c (after insert, after update) {

    List<PostNewProduct> newProducts = new List<PostNewProduct>();
    //List items will be sent
    List<PBSI__PBSI_Item__c> sentItems = new List<PBSI__PBSI_Item__c>();
    for(PBSI__PBSI_Item__c item : Trigger.New) {
        if(item.Is_Fulfilled__c && (item.Post_to_DocData__c == null || String.isEmpty(item.Post_to_DocData__c))) {
            PostNewProduct product = new PostNewProduct();
            product.Sku = item.Name;
            product.Barcode = item.Barcode_Number__c;
            //product.Barcode = item.EAN__c;
            product.Description = item.PBSI__description__c;
            product.Supplier ='' ;//item.PBSI__Default_Vendor__c;
            product.Category ='';// item.PBSI__Item_Group__r.name;
            product.Height = String.valueOf(item.PBSI__height__c);
            product.Width = String.valueOf(item.PBSI__width__c);
            product.Depth = String.valueOf(item.PBSI__depth__c);
            product.Value =0;// item.PBSI__salesprice__c;
            product.RetailValue = 0;//item.PBSI__salesprice__c;
            
            newProducts.add(product);
            PBSI__PBSI_Item__c i = [select id, name, Is_Fulfilled__c, Post_to_DocData__c, Post_to_DocData_Date__c from PBSI__PBSI_Item__c where id = :item.Id];
            sentItems.add(i); 
        }       
    }
    //post to docdata
    if(!newProducts.isEmpty()) {
        //List items will be sent
        String items = Json.serialize(sentItems);
        
        String products = Json.serialize(newProducts);
        ODIWebServices.postProducts(products, items);        
    }
    
}