/*
*       https://github.com/abhinavguptas/Salesforce-Lookup-Rollup-Summaries
*
*
*/
trigger Inventory_BeforeAfter on PBSI__PBSI_Inventory__c (after delete, after update, after insert, after undelete) {

    // modified objects whose parent records should be updated
    PBSI__PBSI_Inventory__c[] objects = null;   

    if (Trigger.isDelete) {
        objects = Trigger.old;
    } else {    
        /*
            Handle any filtering required, specially on Trigger.isUpdate event.            
        */ 
        //for (PBSI__PBSI_Inventory__c inv : Trigger.new ){

        //}
        objects = Trigger.new;
    }

    /*
      First step is to create a context for LREngine, by specifying parent and child objects and
      lookup relationship field name
     */
    LREngine.Context ctx = new LREngine.Context(PBSI__PBSI_Item__c.SobjectType, // parent object
                                            PBSI__PBSI_Inventory__c.SobjectType,  // child object
                                            Schema.SObjectType.PBSI__PBSI_Inventory__c.fields.PBSI__item_lookup__c, // relationship field name
                                            'PBSI__location_lookup__r.PBSI__Count_in_Inventory__c = \'Yes\' AND PBSI__location_lookup__r.PBSI__In_Transit__c = False AND PBSI__location_lookup__r.Name != \'DocData\' ' 
                                            );    
    
    /*
      Next, one can add multiple rollup fields on the above relationship. 
      Here specify 
       1. The field to aggregate in child object
       2. The field to which aggregated value will be saved in master/parent object
       3. The aggregate operation to be done i.e. SUM, AVG, COUNT, MIN/MAX
     */
    ctx.add(
            new LREngine.RollupSummaryField(
                                            Schema.SObjectType.PBSI__PBSI_Item__c.fields.Quantity_on_Hand_at_OBTC__c,
                                            Schema.SObjectType.PBSI__PBSI_Inventory__c.fields.PBSI__qty__c,
                                            LREngine.RollupOperation.Sum 
                                         )); 

     LREngine.Context ctxDocData = new LREngine.Context(PBSI__PBSI_Item__c.SobjectType, // parent object
                                            PBSI__PBSI_Inventory__c.SobjectType,  // child object
                                            Schema.SObjectType.PBSI__PBSI_Inventory__c.fields.PBSI__item_lookup__c, // relationship field name
                                            'PBSI__location_lookup__r.PBSI__Count_in_Inventory__c = \'Yes\' AND PBSI__location_lookup__r.Name = \'DocData\' ' 
                                            );    

     ctxDocData.add(
            new LREngine.RollupSummaryField(
                                            Schema.SObjectType.PBSI__PBSI_Item__c.fields.Quantity_on_Hand_at_DocData__c,
                                            Schema.SObjectType.PBSI__PBSI_Inventory__c.fields.PBSI__qty__c,
                                            LREngine.RollupOperation.Sum 
                                         )); 
    /* 
      Calling rollup method returns in memory master objects with aggregated values in them. 
      Please note these master records are not persisted back, so that client gets a chance 
      to post process them after rollup
      */          
     Sobject[] masters = LREngine.rollUp(ctx, objects);  
     Sobject[] mastersDocData = LREngine.rollUp(ctxDocData, objects);  

     // Persiste the changes in master
     update masters; 
     update mastersDocData;

}