trigger TriggerPalletInformation on Pallet_Information__c (after insert, after update, after delete) {
    List<id> listSalesOrderId = new List<id>(); 
    if( trigger.isinsert || trigger.isupdate)
    {
        for( Pallet_Information__c newItem : trigger.new )
        {
            listSalesOrderId.add(newItem.Sales_Order__c);
        }
    }
    if( trigger.isdelete)
    {
        for(Pallet_Information__c deleteItem : trigger.old)
        {
            listSalesOrderId.add(deleteItem.Sales_Order__c);
        }
    }
    
    List<PBSI__PBSI_Sales_Order__c> listSaleOrder = [select Id,Name,	PalletLine__c 
                                                     from PBSI__PBSI_Sales_Order__c where id in:  listSalesOrderId];
    List<Pallet_Information__c> listPalletInfor = [select Id,Name,Total_Number_of_Pallets_Cartons__c, Pallet_Carton__c,Sales_Order__c 
                                                   from Pallet_Information__c where 	Sales_Order__c in: listSalesOrderId  ];
    for( PBSI__PBSI_Sales_Order__c itemSO : listSaleOrder )
    {
        String temp = '';
        for( Pallet_Information__c itemPI :listPalletInfor  )
        {
            if(itemPI.Sales_Order__c == itemSO.Id)
            {
                 if( temp != '')
                	temp+= ', ';
            	temp += itemPI.Total_Number_of_Pallets_Cartons__c + ' of ' + itemPI.Pallet_Carton__c;
            }
        }
        if( temp != '')
            temp = '(' + temp + ')';
        itemSO.PalletLine__c = temp;
    }
    update listSaleOrder;
}