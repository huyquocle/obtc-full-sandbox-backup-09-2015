@istest (seeAllData=true)
public class TriggerOpportunityUpdateShippingTest {
    static testmethod void test_trigger(){
        Account thisAcc = new Account();
        
        thisAcc.Description = 'Test customer site';
        thisAcc.FAX = '555-1234';
        
        thisAcc.Industry = null;
        
        thisAcc.Name = 'Test Customer Paul';
        thisAcc.NumberOfEmployees = null;
        thisAcc.Phone = null;
        thisAcc.Ownership = null;
        thisAcc.ParentId = null;
        
        thisAcc.TickerSymbol = null;
        thisAcc.Type = null;
        thisAcc.Website = null;
        
        
        thisAcc.BillingStreet = '123 Main St\nSuite 101\nc/0George';
        thisAcc.BillingCity = 'San Francisco';
        thisAcc.BillingState = 'CA';
        thisAcc.BillingPostalCode = '12345';
        thisAcc.BillingCountry = 'US';
        
        thisAcc.ShippingStreet = '123 Main St\nSuite 101\nc/0George';
        thisAcc.ShippingCity = 'San Francisco';
        thisAcc.ShippingState = 'CA';
        thisAcc.ShippingPostalCode = '12345';
        thisAcc.ShippingCountry = 'US';
        
        insert thisAcc;
        
        //Contact thisCon = tc.createTestCustomerContact(true, thisAcc);
        
        Contact thisCon = new Contact();
        
        thisCon.AccountId = thisAcc.id;
        thisCon.AssistantPhone = null;
        thisCon.AssistantName = null;
        thisCon.Birthdate = null;
        thisCon.Department = null;
        thisCon.Description = null;
        thisCon.Email = 'test@aol.com';
        thisCon.FAX = null;
        thisCon.FirstName = 'Test';
        thisCon.HomePhone = null;
        //thisCon.SCMC__Language__c = null;
        thisCon.MailingCity = null;
        thisCon.MailingCountry = null;
        thisCon.MailingState = null;
        thisCon.MailingStreet = null;
        thisCon.MailingPostalCode = null;
        thisCon.MobilePhone = null;
        thisCon.Phone = '9015888555';
        thisCon.OtherPhone = null;
        thisCon.Salutation = 'Mr.';
        
        thisCon.ReportsTo = null;
        
        thisCon.Title = null;
        thisCon.LastName = 'Lastname';
        
        insert thisCon;
        
        
        
        //Opportunity thisOpp = tc.createTestOpportunity(thisAcc, false);
        Opportunity thisOpp = new Opportunity(
            Name = 'TestOpp1'
            , StageName = 'New'
            , CloseDate = system.today()
            , AccountId = thisAcc.id);
        
        List<PBSI__PBSI_Item__c> listItem = [select Id, Name from PBSI__PBSI_Item__c limit 1];
        
        thisOpp.CurrencyIsoCode = 'GBP';
        thisOpp.Company_Name__c = 'Original BTC';
        insert thisOpp;
        
        thisOpp.Billing_Contact__c = thisCon.id;
        thisOpp.Web_Site_Order__c = true;
        update thisOpp;
        
        Product2 dummyProduct = new Product2();
        dummyProduct.Name = 'Test Product';
        dummyProduct.Ascent4Prods__Item__c = listItem[0].id;
        insert dummyProduct;
        
        Pricebook2 orgPriceBook = [Select Id From Pricebook2 Where Name like '%Standard%' LIMIT 1];
        
        PricebookEntry dummyPricebookEntry = new PricebookEntry();
        dummyPricebookEntry.IsActive = true;
        dummyPricebookEntry.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry.Product2Id = dummyProduct.Id;
        dummyPricebookEntry.UnitPrice = 10000;
        dummyPricebookEntry.CurrencyIsoCode = 'GBP';
        insert dummyPricebookEntry;
        
        
        
        PricebookEntry dummyPricebookEntry2 = new PricebookEntry();
        dummyPricebookEntry2.IsActive = true;
        dummyPricebookEntry2.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry2.Product2Id = dummyProduct.Id;
        dummyPricebookEntry2.UnitPrice = 10000;
        dummyPricebookEntry2.CurrencyIsoCode = 'EUR';
        insert dummyPricebookEntry2;
        
        PricebookEntry dummyPricebookEntry3 = new PricebookEntry();
        dummyPricebookEntry3.IsActive = true;
        dummyPricebookEntry3.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry3.Product2Id = dummyProduct.Id;
        dummyPricebookEntry3.UnitPrice = 10000;
        dummyPricebookEntry3.CurrencyIsoCode = 'USD';
        
        insert dummyPricebookEntry3;
        
        
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = thisOpp.Id;
        //oli.CurrencyIsoCode = 'USD';
        oli.PricebookEntryId = dummyPricebookEntry.Id;
        oli.UnitPrice = 123.99;
        oli.Quantity = 1;
        
        insert oli;

        
          PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        //salesOrder.Name = 'test';
        salesOrder.PBSI__Opportunity__c = thisopp.id;
        insert salesOrder;
        
        
        // get item limit 1
        
        PBSI__PBSI_Sales_Order_Line__c saleSorderLine = new PBSI__PBSI_Sales_Order_Line__c();
        saleSorderLine.PBSI__Item__c = listItem[0].id;
        saleSorderLine.PBSI__Sales_Order__c = salesOrder.id;
        saleSorderLine.PBSI__Quantity__c = 1;
        saleSorderLine.PBSI__Price__c = 100;
        insert saleSorderLine;
        
        thisOpp.StageName = 'Closed Won';
        update thisOpp;
        
        
    }
    
}