@isTest(seealldata = true)
global class TestSendNewOrdersToDocData implements HttpCalloutMock{

    private HttpResponse resp;
    public TestSendNewOrdersToDocData(String testResponse) {
        resp = new HttpResponse();
        resp.setBody(testResponse);
        resp.setStatusCode(200);
        resp.setStatus('OK');
    }
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static HttpResponse doInvoke() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://api.salesforce.com/foo/bar?id=');
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        return res;
    }
    
    global static testMethod void sendToDocData() {
        //set up test data
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = system.today();
        testOpportunity.StageName = 'Proposal/Price Quote';
        testOpportunity.Estimated_Dispatch_Date__c = system.today();
        testOpportunity.Shipper__c = 'hello world';
        testOpportunity.Wired_For__c = 'UK';
        testOpportunity.Packing__c = 'Individually boxed';
        testOpportunity.Customer_Order_Number__c = 'Customer_Order_Number__c';
        testOpportunity.Required_For__c = system.today();
        testOpportunity.Description = 'description';
        testOpportunity.Bespoke__c = true;
        testOpportunity.Urgent__c = true;
        testOpportunity.Call_Email_Before_Dispatch__c = true;
        testOpportunity.Sooner_Delivery_If_Possible__c = true;
        testOpportunity.Samples__c = true;
        testOpportunity.Allow_Return__c = true;
        testOpportunity.Combine_with_Order__c = 'combine with order';
        testOpportunity.Dispatch_Method__c = 'Customer pick-up';
        testOpportunity.Fulfilment__c = 'Fulfillment';
        testOpportunity.Website_fulfilment_order__c = true;
        insert testOpportunity;
        
        PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        salesOrder.PBSI__Customer__c = testAccount.Id;
        salesOrder.PBSI__Opportunity__c = testOpportunity.Id;
        salesOrder.PBSI__Order_Date__c = system.today();
        salesOrder.PBSI__Due_Date__c = system.today() + 15;
        salesOrder.Sent_To_DC__c = false;
        insert salesOrder;       
        
        PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c limit 1];
        
        PBSI__PBSI_Sales_Order_Line__c line = new PBSI__PBSI_Sales_Order_Line__c();
        line.PBSI__Item__c = item.id;
        line.PBSI__Sales_Order__c = salesOrder.Id;
        line.PBSI__Quantity_Needed__c = 3;
        line.PBSI__Price__c = 200;
        insert line;
        
        Test.startTest();
        
       	//String testResponse = '{"Orders":[{"Key":"'+salesOrder.Name+'","Value":9}],"Total":1}';
        String testResponse = '{"Orders":[{"OrderRef":"'+salesOrder.Name+'","DbIndex":1,"Status":"I"}],"Total":1}';
       	HttpCalloutMock mock = new TestSendNewOrdersToDocData(testResponse);
       	Test.setMock(HttpCalloutMock.class, mock);
        HttpResponse res = doInvoke();
        
        SendNewOrdersToDocData b = new SendNewOrdersToDocData();
        Database.executeBatch(b, 10);
        Test.stopTest();
    }
}