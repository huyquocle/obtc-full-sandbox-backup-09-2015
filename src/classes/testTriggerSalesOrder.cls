@istest(SeeAllData=true)
public class testTriggerSalesOrder {
	public static testMethod void testSalesOrder()
    {
		c2g__codaGeneralLedgerAccount__c legerAccount = new c2g__codaGeneralLedgerAccount__c();
        legerAccount.Name = 'test general';
        legerAccount.c2g__ReportingCode__c = '123';
        legerAccount.CurrencyIsoCode = 'EUR';
        legerAccount.c2g__Type__c = 'Balance Sheet';
        insert legerAccount;
        
        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();
        taxCode.Name = '12345678';
        taxCode.c2g__Description__c = 'Description';
        taxCode.CurrencyIsoCode = 'EUR';
        taxCode.c2g__GeneralLedgerAccount__c = legerAccount.id;
        insert taxCode;
        
        Account  account = new Account ();
        account.Name = 'test account';
		insert account; 
        
        Opportunity opp = new Opportunity ();
        opp.Name = 'test';
        opp.Account = account;
        opp.CloseDate = system.Date.today();
        opp.StageName = 'Prospecting';
        opp.Shipping_and_Handling__c = 100;
        opp.VAT_Code__c = '12345678';
        insert opp;
        
        PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        //salesOrder.Name = 'test';
        salesOrder.PBSI__Opportunity__c = opp.id;
        insert salesOrder;
        
        
        // get item limit 1
        List<PBSI__PBSI_Item__c> listItem = [select Id, Name from PBSI__PBSI_Item__c limit 1];
        PBSI__PBSI_Sales_Order_Line__c saleSorderLine = new PBSI__PBSI_Sales_Order_Line__c();
        saleSorderLine.PBSI__Item__c = listItem[0].id;
        saleSorderLine.PBSI__Sales_Order__c = salesOrder.id;
        saleSorderLine.PBSI__Quantity__c = 1;
        saleSorderLine.PBSI__Price__c = 100;
        try
        {
			insert saleSorderLine;            
        }
        catch( System.Exception  er)
        {}
        
        c2g__codaInvoice__c sin = new c2g__codaInvoice__c();
        sin.Ascent2FF__Sales_Order__c = salesOrder.Id;
        sin.c2g__Account__c = account.Id;

        insert sin;
        
       
    }
    
   
    
    
}