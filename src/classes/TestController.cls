public with sharing class TestController{
    PBSI__PBSI_Sales_Order__c so;
    public Id cnid  {set;get;}
    
    public TestController(ApexPages.StandardController c)
    {
        cnid = c.getId();
    }
    
    public pagereference test()
    {  
        List<PBSI__PBSI_Sales_Order_Line__c> solines = [Select PBSI__Item__c from PBSI__PBSI_Sales_Order_Line__c where PBSI__Sales_Order__c =:cnid];
        List<PBSI__BOM_Depletion_Line__c> depl = [Select PBSI__Item__c from PBSI__BOM_Depletion_Line__c where PBSI__Sales_Order__c =:cnid];
        Set<Id> ids = new Set<Id>();
        for (PBSI__PBSI_Sales_Order_Line__c s:solines)
            ids.add(s.PBSI__Item__c);
        for (PBSI__BOM_Depletion_Line__c d:depl)
            ids.add(d.PBSI__Item__c);
        Map<String, Decimal> openBDLinesAllocQtyByItemIDsMap = getOpenBDLinesAllocQtyByItemIDsMap(ids);       
        
    return null;
    }

    public Map<String, Decimal> getOpenBDLinesAllocQtyByItemIDsMap(Set<Id> itemIDs)
    {
        PBSI__Admin__c admin = PBSI__Admin__c.getOrgDefaults();
        
        Map<String, Decimal> bdlAllocQtyByItemMap = new Map<String, Decimal>();
        
        Integer partsNr = 1;
        
        if(admin != null && admin.PBSI__AllocQty_by_Bd_Lines_Starting_Queries_Nr__c != null)
            partsNr = Integer.valueOf(admin.PBSI__AllocQty_by_Bd_Lines_Starting_Queries_Nr__c);
            
        if(partsNr > itemIDs.size())    
            partsNr = itemIDs.size();
        Boolean doWhile = true;
        system.debug('kkk11='+partsNr);
        system.debug('kkk111='+itemIDs.size());
        Boolean isError = getOpenBDLinesAllocQtyByItemIDs(itemIDs, bdlAllocQtyByItemMap, partsNr);
        return bdlAllocQtyByItemMap;
    }
    private List<Set<Id>> GetItemsSetPart(Set<Id> itemIDs, Integer oneSetMaxSize)
    {
        List<Set<Id>> itemSetParts = new List<Set<Id>>();
        Integer addedPartSetNr = 0;
        Integer addedAllNr = 0;
        Set<Id> itemSetPart;
         
        if(Test.isRunningTest())
             oneSetMaxSize = itemIDs.size();
             
        for(Id itemID: itemIDs)
        {
             if(addedPartSetNr == 0) 
                itemSetPart = new Set<Id>();
                
             itemSetPart.add(itemID);
             
             addedPartSetNr ++;
             addedAllNr ++;
             
             if(addedPartSetNr == oneSetMaxSize || addedAllNr == itemIDs.size())
             {
                addedPartSetNr = 0;
                itemSetParts.add(itemSetPart);
             }
        }
        
        return itemSetParts;
    }
    public pagereference backtocn()
    {
        string url;
        url='/' + cnid;
        PageReference so=new PageReference(url);
        return so;
    }  
    
    public Boolean getOpenBDLinesAllocQtyByItemIDs(Set<Id> itemIDs, Map<String, Decimal> bdlAllocQtyByItemMap, Integer itemsSetPartSize)
    {
         PBSI__Admin__c admin=PBSI__Admin__c.getOrgDefaults();
         Integer oneSetMaxSize = math.round(itemIDs.size() / itemsSetPartSize);
         List<Set<Id>> itemSetParts = GetItemsSetPart(itemIDs, oneSetMaxSize);
         system.debug('ppp1='+itemIDs.size());
         system.debug('ppp2='+itemsSetPartSize);
         system.debug('ppp3='+itemSetParts);
         List<string> statuses = new List<string>{ 'Closed', 'Cancelled' };
         List<string> sotypes = new List<string>();
         
         if (admin <> null && admin.PBSI__SO_Statuses_not_included_in_Alloc_Qty__c <> null)
             statuses.addALL(admin.PBSI__SO_Statuses_not_included_in_Alloc_Qty__c.split(';'));
    
         if (admin <> null && admin.PBSI__SO_Types_not_included_in_Alloc_Qty__c <> null)
             sotypes.addALL(admin.PBSI__SO_Types_not_included_in_Alloc_Qty__c.split(';'));
         
         String strKey = admin != null && admin.PBSI__Is_Indexed_Item_ID_Available__c == true ? 'Indexed_Item_ID__c' : 'PBSI__Item__c';
         //try
         //{
            String strQuery;
            
            // IT 7/17/2015
            Set<Id> idsforitemsparts = new Set<Id>();
            for(Set<Id> itemsSetPart: itemSetParts)
                for (Id i:itemsSetPart)
                    idsforitemsparts.add(i);
                         
            //for(Set<Id> itemsSetPart: itemSetParts)
            //{
                 strQuery = '';
         
                 if(admin != null && admin.PBSI__Is_Indexed_Item_ID_Available__c == true)
                 {
                        strQuery += ' SELECT ' +
                                    '     Indexed_Item_ID__c, ' +
                                    '     SUM(PBSI__Quantity_Left__c) qtyLeft ';
                 }   
                 else
                 {
                        strQuery += ' SELECT ' +
                                    '     PBSI__Item__c, ' +
                                    '     SUM(PBSI__Quantity_Left__c) qtyLeft ';
                 } 
                
                 strQuery += ' FROM ' +
                             '     PBSI__BOM_Depletion_Line__c '                             +
                             ' WHERE ' +
                             ' ( ' +
                             '   ( ' +
                             '      PBSI__Purchase_Order__c <> NULL ' +
                             '        AND ' +
                             '      PBSI__Purchase_Order_Line__r.PBSI__LineStatus__c = \'Open\' ' +
                             '   ) ' +
                             '   OR ' +
                             '   ( ' + 
                             '      PBSI__Sales_Order__c <> NULL ' +
                             '        AND ' +
                             '      PBSI__Sales_Order_Line__r.PBSI__Line_Status__c = \'Open\' ' + 
                             '        AND ' + 
                             '      PBSI__Sales_Order_Line__r.PBSI__TransactionType__c = \'sale\' ' + 
                             '        AND ' + 
                             '      PBSI__Sales_order__r.PBSI__Status__c not in :statuses ' + 
                             '        AND ' +
                             '      PBSI__Sales_order__r.PBSI__Type__c not in :sotypes ' +
                             '        AND ' +
                             '       PBSI__Sales_Order_Line__r.PBSI__Drop_Ship_PO__c=null ' +
//                             '        OR ' +
  //                           '       PBSI__Purchase_Order__r.PBSI__Drop_Ship_Sales_Order__c = null )' +
                             '   ) ' +
                             '   OR ' +
                             '   ( ' +
                             '      PBSI__Production_Work_Order__c <> null ' +
                             '         AND ' +
                             '      ( ' +
                             '            PBSI__Production_Work_Order__r.PBSI__Status__c = \'Open\' ' +
                             '               OR ' +
                             '            PBSI__Production_Work_Order__r.PBSI__Status__c = \'In Progress\' ' +
                             '      ) ' +
                             '   ) ' + 
                             ' ) ' +
                             ' AND ' +
                             '     PBSI__Quantity_Left__c > 0 ';
                 if(admin != null && admin.PBSI__Is_Indexed_Item_ID_Available__c == true)
                 {
                     strQuery += ' AND ' + 
//                                 '   Indexed_Item_ID__c IN: itemsSetPart ' +
                                 '   Indexed_Item_ID__c IN: idsforitemsparts ' +
                                 ' GROUP BY ' + 
                                 '   Indexed_Item_ID__c ';
                 }
                 else
                 {
                    strQuery += ' AND ' + 
//                                '   PBSI__Item__c IN: itemsSetPart ' +
                                '   PBSI__Item__c IN: idsforitemsparts ' +
                                ' GROUP BY ' + 
                                 '   PBSI__Item__c ';
                 }
                 
                 List<AggregateResult> openBDLines = Database.Query(strQuery);  
                 
                 if(openBDLines.size() > 0)
                 {
                     for (AggregateResult aggrRecord : openBDLines)
                     {
                         system.debug('ppp4='+(String)aggrRecord.get(strKey));
                         system.debug('ppp5='+(Decimal)aggrRecord.get('qtyLeft'));
                         bdlAllocQtyByItemMap.put((String)aggrRecord.get(strKey), (Decimal)aggrRecord.get('qtyLeft'));
                     }
                 }
                 //if(Test.isRunningTest())                    break;       
             //}
         //}
         //catch(Exception ex)
         //{
          // bdlAllocQtyByItemMap.clear();
           //return true;
         //}
         return false;
    }
      
}