@isTest(seealldata = true)
public class TestStockAdjustRestService {

    static testMethod void adjust() {
        
      	//set up test data
      	
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
        insert itemGroup;
        
        PBSI__PBSI_Location__c location = [select id, name from PBSI__PBSI_Location__c where name = 'Default Location' limit 1];
        
        //PBSI__PBSI_Location__c docdata = [select id, name from PBSI__PBSI_Location__c where name = 'DocData' limit 1];
        List<PBSI__PBSI_Location__c> docdatas=[select id from PBSI__PBSI_Location__c where name = 'DocData'];
        PBSI__PBSI_Location__c docdata;
        if(docdatas == null || docdatas.size() == 0) {
            docdata = new PBSI__PBSI_Location__c(Name='DocData');  
        	insert docdata;
        } else {
            docdata = docdatas[0];
        }
        PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c where PBSI__Default_Location__r.name = 'Default Location' limit 1];
        PBSI__Lot__c fromLot = [select id, name from PBSI__Lot__c where Name = 'Default Location' and PBSI__Item__c = :item.id limit 1];
        system.debug('#fromLot#' + fromLot);
        /*
        PBSI__Lot__c fromLot = new PBSI__Lot__c(Name='Default Location');
        fromLot.PBSI__Item__c = item.id;
        fromLot.PBSI__Location__c = location.id;
        insert fromLot;
        */
        PBSI__PBSI_Inventory__c inven = new PBSI__PBSI_Inventory__c();
        inven.PBSI__location_lookup__c = location.Id;
        inven.PBSI__item_lookup__c = item.Id;
        inven.PBSI__qty__c = 100;
        insert inven;
        system.debug('#inventory#' + inven);
        List<PBSI__Movement_Journal__c> mjs = [select id, PBSI__Type__c from PBSI__Movement_Journal__c where PBSI__Type__c = 'Transfer Posting Location to Location'];
        PBSI__Movement_Journal__c mj;
        if(mjs == null || mjs.size() == 0) {
            mj = new PBSI__Movement_Journal__c();
        	mj.PBSI__Type__c = 'Transfer Posting Location to Location';
        	mj.PBSI__Movement_Type__c = 500;
        	insert mj;
        } else {
            mj = mjs[0];
        }
         
        PBSI__Movement_Line__c ml = new PBSI__Movement_Line__c();
        ml.PBSI__Item__c = item.Id;
        ml.PBSI__Transfer_Quantity__c = 60;
        ml.PBSI__From_Location__c = location.id;
        ml.PBSI__To_Location__c = docdata.Id;
        ml.PBSI__Transaction_Date__c = system.now();
        ml.PBSI__Movement_Journal__c = mj.id;
        ml.PBSI__Lot__c = fromLot.Id;
        insert ml;
        
       Test.startTest();
        
        StockAdjustRestService.ItemRequest request = new StockAdjustRestService.ItemRequest();
        request.SKU = item.Name;
        request.Quantity = 20;
        request.ReasonCode = 'Theft';
        //request.Description = 'description';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        StockAdjustRestService.adjust(request);
        
        StockAdjustRestService.ItemRequest request2 = new StockAdjustRestService.ItemRequest();
        request2.SKU = item.Name;
        request2.Quantity = 20;
        request2.ReasonCode = 'Found';
        //request.Description = 'description';
        
        RestContext.request = req;
        RestContext.response = res;
        StockAdjustRestService.adjust(request2);
    }
}