public class TriggerUtiSO {
    
    public static void UpdateLotForFulfillmentSalesOrderLines(List<PBSI__PBSI_Sales_Order_Line__c> soLineList, boolean requireUpdate) {
        Set<ID> itemIds = new Set<Id>();
        for (PBSI__PBSI_Sales_Order_Line__c soLine : soLineList) {
            if (soLine.PBSI__Item__c != NULL) {
                itemIds.add(soLine.PBSI__Item__c);
            }
        }
        
        Map<Id, PBSI__PBSI_Item__c> mapItems = new Map<Id, PBSI__PBSI_Item__c>([SELECT Id, Name, PBSI__Item_Type__c FROM PBSI__PBSI_Item__c WHERE Id IN :itemIds]);
        List<PBSI__Lot__c> LotList = [SELECT ID, Name, PBSI__Item__c FROM PBSI__Lot__c WHERE PBSI__Item__c IN :itemIds AND (Name = 'Shipped from DocData' OR Name = 'DocData')];
        Map<string, PBSI__Lot__c> mapLots =new Map<string, PBSI__Lot__c>();
        
        for (PBSI__Lot__c lot : LotList) {
            mapLots.put(lot.Name + lot.PBSI__Item__c, lot);
        }
        
        system.debug('#mapLots#' + mapLots);
        
        List<PBSI__PBSI_Sales_Order_Line__c> updateLines = new List<PBSI__PBSI_Sales_Order_Line__c>();
        for (PBSI__PBSI_Sales_Order_Line__c soLine : soLineList) {
            if (soLine.PBSI__Item__c != NULL) {
                PBSI__PBSI_Item__c lineItem = mapItems.get(soLine.PBSI__Item__c);
                string mapLotKey = 'Shipped from DocData';
                if (lineItem.PBSI__Item_Type__c == 'Item') {
                    mapLotKey = 'DocData';
                }
                mapLotKey += lineItem.Id;
                
                system.debug('#mapLotKey#' + mapLotKey);
                
                PBSI__Lot__c lot = mapLots.get(mapLotKey);
                if (lot != NULL) {
                    soLine.PBSI__Lot__c = lot.Id;
                    updateLines.add(soLine);
                }
            }
        }
        system.debug('#updateLines#' + updateLines);
        if (requireUpdate) {
            update updateLines;
        }
    }
    
    public static void CreateFFInvoice(list<PBSI__PBSI_Sales_Order__c> lstSO){
        
        list<PBSI__PBSI_Sales_Order__c> lstSO_Items = [Select Id,PBSI__Opportunity__c,PBSI__Customer__c,Name,CurrencyIsoCode, Invoice_Generated__c,
                                                       PBSI__Opportunity__r.Opportunity_ID__c,
                                                       PBSI__Opportunity__r.Company_Name__c,
                                                       PBSI__Opportunity__r.VAT_Code__c,
                                                       PBSI__Opportunity__r.Accountid,
                                                       (Select Id, Name, PBSI__Price__c, PBSI__Quantity__c,CurrencyIsoCode, PBSI__Tax__c, PBSI__Total_Price__c,
                                                        PBSI__Item__r.PBSI__Product__c,PBSI__Quantity_Needed__c 
                                                        From PBSI__Sales_Order_Lines__r) 
                                                       From PBSI__PBSI_Sales_Order__c where id in:lstSO];
        
        list<c2g__codaInvoice__c> lstFFIV = new list<c2g__codaInvoice__c>();
        list<c2g__codaInvoiceLineItem__c> lstILI = new list<c2g__codaInvoiceLineItem__c>();
        map<string,id> map_Id_Currency = new map<string,id>();
        //Put Currency to Map
        for(c2g__codaAccountingCurrency__c curr : [select id, name from c2g__codaAccountingCurrency__c]){
            map_Id_Currency.put(curr.Name,curr.id);
        }
        map<id,c2g__codaInvoice__c> mapId_Invoice = new map<id,c2g__codaInvoice__c>();
        
        //Get mapping company picklist opportunity --> ff company.
        List<c2g__codaCompany__c> companyList = [select id,Name 
                                                 From c2g__codaCompany__c 
                                                 WHERE Name in ('Original BTC Limited',
                                                                'Eurl Original BTC France',
                                                                'The Lighting Library Limited')];
        
        Map<String,String> companyMapping = new Map<String,string>();
        set<id> companyIds = new set<id>();
        for(c2g__codaCompany__c item :companyList )
        {
            system.debug('***** Comp Name: ' + item.Name);
            if(item.Name == 'Original BTC Limited')
            {
                companyMapping.put('LM England', item.id);
                companyMapping.put('Original BTC', item.id);          
            }
            else if(item.Name == 'Eurl Original BTC France')
            {
                companyMapping.put('LM France', item.id);
                companyMapping.put('Original BTC France', item.id);          
            }
            else if(item.Name == 'The Lighting Library Limited')
            {
                companyMapping.put('The Lighting Library', item.id);   
            }            
            companyIds.add(item.id);
        }
        // End company mapping 
        
        //Get User Company based on company set id
        List<c2g__codaUserCompany__c> userCompany =[select c2g__User__c,c2g__Company__c 
                                                    FROM   c2g__codaUserCompany__c where c2g__Company__c in:companyIds];
        set<id> userids = new set<id>();
        for(c2g__codaUserCompany__c item: userCompany)
        {
            userids.add(item.c2g__User__c);
        }
       
        //Get Queue based on user 
        Map<id,id> userQueueIDs = new MAp<id,id>();
        List<GroupMember> queueList =[ SELECT GroupId,UserOrGroupId FROM GroupMember 
                                                                    WHERE UserOrGroupId in: userids 
                                                                    AND Group.Type = 'Queue'];
       
        for(GroupMember gr:queueList)
        {
            if(userQueueIDs.get(gr.UserOrGroupId)==null)
            {
                userQueueIDs.put(gr.UserOrGroupId,gr.GroupId);
            }
        }
        Set<string> taxCode = new set<String>();
        for(PBSI__PBSI_Sales_Order__c so: lstSO_Items)
        {
            taxCode.add(so.PBSI__Opportunity__r.VAT_Code__c);
        }
        Map<string,id> taxCodeNameId = new Map<String,id>();
        for(c2g__codaTaxCode__c txitem:[select id, name from c2g__codaTaxCode__c where name in:taxCode])
        {
            if(taxCodeNameId.get(txitem.Name) == null)
            {
                taxCodeNameId.put(txitem.Name,txitem.id);
            }
        }       
        for(PBSI__PBSI_Sales_Order__c so: lstSO_Items){
            system.debug('***** so.PBSI__Customer__c: ' + so.PBSI__Customer__c);
        
            c2g__codaInvoice__c ffiv = new c2g__codaInvoice__c();
            ffiv.c2g__Opportunity__c = so.PBSI__Opportunity__c;
            ffiv.c2g__Account__c = so.PBSI__Opportunity__r.Accountid;
            ffiv.c2g__InvoiceDate__c = system.Today();
            ffiv.c2g__DueDate__c = system.Today();             
            ffiv.c2g__CustomerReference__c =  so.PBSI__Opportunity__r.Opportunity_ID__c;
            
            //system.debug('***** Opp Company Name: ' + so.PBSI__Opportunity__r.Company_Name__c);
            //string coId = companyMapping.get(so.PBSI__Opportunity__r.Company_Name__c);            
            //system.debug('***** coId : ' + coId );
            //ffiv.c2g__OwnerCompany__c = coId ;
         
            ffiv.c2g__TaxCode1__c = taxCodeNameId.get( so.PBSI__Opportunity__r.VAT_Code__c);
            ffiv.c2g__InvoiceCurrency__c = map_Id_Currency.get(so.CurrencyIsoCode);            
            ffiv.Ascent2FF__Sales_Order__c = so.id;
            lstFFIV.add(ffiv);
            mapId_Invoice.put(so.id,ffiv);
            so.Invoice_Generated__c = True;
        }
        if(!Test.IsRunningTest()) {
            system.debug('#lstFFIV#=' + lstFFIV);
            insert lstFFIV;
        }
        
        for(PBSI__PBSI_Sales_Order__c so: lstSO_Items){
            c2g__codaInvoice__c ffiv = mapId_Invoice.get(so.id);
            for(PBSI__PBSI_Sales_Order_Line__c sol: so.PBSI__Sales_Order_Lines__r ){
                c2g__codaInvoiceLineItem__c IvLi = new c2g__codaInvoiceLineItem__c();
                IvLi.c2g__Invoice__c = ffiv.id;
                IvLi.c2g__Quantity__c = sol.PBSI__Quantity_Needed__c;
                IvLi.CurrencyIsoCode = sol.CurrencyIsoCode;
                IvLi.c2g__DeriveUnitPriceFromProduct__c = false;
                IvLi.c2g__UnitPrice__c = sol.PBSI__Price__c;
                IvLi.c2g__TaxRate1__c = sol.PBSI__Tax__c;
                IvLi.c2g__Product__c = sol.PBSI__Item__r.PBSI__Product__c;
                lstILI.add(IvLi);
            }
        }
        if(!Test.IsRunningTest()){
            insert lstILI;
        }
        
        update lstSO_Items;
        
    }
}