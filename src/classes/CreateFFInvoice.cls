global class CreateFFInvoice implements  Database.Batchable<sObject>, Database.AllowsCallouts{ 
    
    global Database.QueryLocator start(Database.BatchableContext info){ 
        return  Database.getQueryLocator([select id,Name,Invoice_Generated__c
                                          FROM PBSI__PBSI_Sales_Order__c 
                                          WHERE id not in (select Ascent2FF__Sales_Order__c from c2g__codaInvoice__c  ) 
                                          AND  PBSI__Stage__c ='Ready for Dispatch' AND Invoice_Generated__c != True AND PBSI__Opportunity__r.Company_Name__c = 'Original BTC France']);        
    }     


    global void execute(Database.BatchableContext info, List<PBSI__PBSI_Sales_Order__c> soList){
        set<string> soName = new set<string>();
        Map<String,PBSI__PBSI_Sales_Order__c> soMap = new Map<String,PBSI__PBSI_Sales_Order__c>();
        
        for(PBSI__PBSI_Sales_Order__c item :soList){
            if(!soName.contains(item.name)){
                soName.add(item.name);
                soMap.put(item.name, item);
            }
        }

        // Sometime the invoice does not link to Sales Order, we need to check the Customer Reference
        List<c2g__codaInvoice__c> coda = [select id ,c2g__CustomerReference__c
                                          FROM c2g__codaInvoice__c 
                                          WHERE c2g__CustomerReference__c in:soName ];
        
        for(c2g__codaInvoice__c codaIn:coda){
            if(soMap.get(codaIn.c2g__CustomerReference__c) != null){
                soMap.remove(codaIn.c2g__CustomerReference__c);
            }
        }

        // Process to create invoice
        TriggerUtiSO.CreateFFInvoice(soMap.values());
    }    


    global void finish(Database.BatchableContext info){} 
}