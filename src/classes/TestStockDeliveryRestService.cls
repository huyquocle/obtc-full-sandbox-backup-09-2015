@isTest(seealldata = true)
public class TestStockDeliveryRestService {



    static testMethod void itemUpdate() {
        /*PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
        insert itemGroup;
    
        PBSI__PBSI_Location__c loc=new PBSI__PBSI_Location__c(Name='DocData');  
        insert loc;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Default_Location__c  = loc.ID;
        item.PBSI__Item_Group__c        = itemGroup.ID;
        item.PBSI__description__c = 'description';
        item.Name = 'Test Name 1';
		item.Is_Fulfilled__c = false;
        item.Ascent4Prods__Do_Not_Sync__c = false;
        insert item;

        */
        PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c limit 1];
        
        Stock_Delivery_History__c s = new Stock_Delivery_History__c();
        s.Item__c = item.Id;
        s.SKU__c = 'SKU';
        s.Date_of_Transfer__c = system.now() - 1;
        s.Transfer_Quantity__c = 30;
        insert s;
        
        StockDeliveryRestService.ItemRequest request = new StockDeliveryRestService.ItemRequest();
        request.SKU = item.name;
        request.Quantity = 30;
        request.PO = 'PO';
        request.DateOfReceive = system.now();
        request.Comment = 'Received';
 
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'https://cs17.salesforce.com/services/apexrest/StockDelivery';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        StockDeliveryRestService.ItemUpdate(request);
    }
}