@istest(seealldata=true)
public class MovementJournalTriggers_Test {
	public static testMethod void test()
    {
        List<PBSI__Movement_Journal__c> journal = [select Id,Name,PBSI__Description__c from PBSI__Movement_Journal__c limit 1];
        journal[0].PBSI__Description__c = '';
        update journal;
    }
}