@isTest
public class TestDocDataOrderStatusRestService {

    static testMethod void updateStatus() {
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = system.today();
        testOpportunity.StageName = 'Proposal/Price Quote';
        testOpportunity.Estimated_Dispatch_Date__c = system.today();
        testOpportunity.Shipper__c = 'hello world';
        testOpportunity.Wired_For__c = 'UK';
        testOpportunity.Packing__c = 'Individually boxed';
        testOpportunity.Customer_Order_Number__c = 'Customer_Order_Number__c';
        testOpportunity.Required_For__c = system.today();
        testOpportunity.Description = 'description';
        testOpportunity.Bespoke__c = true;
        testOpportunity.Urgent__c = true;
        testOpportunity.Call_Email_Before_Dispatch__c = true;
        testOpportunity.Sooner_Delivery_If_Possible__c = true;
        testOpportunity.Samples__c = true;
        testOpportunity.Allow_Return__c = true;
        testOpportunity.Combine_with_Order__c = 'combine with order';
        testOpportunity.Dispatch_Method__c = 'Customer pick-up';
        insert testOpportunity;
        
        PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        salesOrder.PBSI__Customer__c = testAccount.Id;
        salesOrder.PBSI__Opportunity__c = testOpportunity.Id;
        salesOrder.PBSI__Order_Date__c = system.today();
        salesOrder.PBSI__Due_Date__c = system.today() + 15;
        insert salesOrder;
    }
}