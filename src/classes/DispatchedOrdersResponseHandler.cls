global class DispatchedOrdersResponseHandler implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    
    public DispatchedOrdersResponseHandler(){}
    global Database.QueryLocator start(Database.BatchableContext dp)
    {
        return null;      
    }    
    global void execute(Database.BatchableContext dp, List<Dispatched_Orders_Response__c> dpo)
    {
        
    }
    global void finish(Database.BatchableContext dpo)
    {
    }
}