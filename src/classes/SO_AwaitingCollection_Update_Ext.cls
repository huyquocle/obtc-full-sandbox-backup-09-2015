public without sharing class SO_AwaitingCollection_Update_Ext {
    
    list<PBSI__PBSI_Sales_Order__c> lstSO = new list<PBSI__PBSI_Sales_Order__c>();
    public SO_AwaitingCollection_Update_Ext(ApexPages.StandardSetController cont){
        lstSO = [select id, PBSI__Stage__c,Locked__c from PBSI__PBSI_Sales_Order__c where id in: cont.getSelected()];
    }
    
    public PageReference updateSO(){
        
        for(PBSI__PBSI_Sales_Order__c pso: lstSO){
            pso.PBSI__Stage__c = 'Awaiting Collection';
            pso.Locked__c = false;
        }
        
        if(lstSO.size() > 0){
            update lstSO;
            }
            
        String returnUrl = ApexPages.currentPage().getParameters().get('retUrl');
        PageReference pageRef = new PageReference(returnUrl);
        
        return pageRef;
    }
}