public class DispatchedOrdersResponseObject {
    
    public List<DispatchedOrdersResponseItem> positionList{set;get;}
    
    public DispatchedOrdersResponseObject()
    {        
    }
    public Static DispatchedOrdersResponseObject getDispatchedOrders(String xml)
    {
        DispatchedOrdersResponseObject returnObject = new DispatchedOrdersResponseObject();
        DOM.Document itemList = new DOM.Document();
        itemList.load(ODIXMLUtils.testData());
        dom.XmlNode root= itemList.getRootElement();
        dom.XmlNode body = root.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
        dom.XmlNode dispatchedOrdersResponse = body.getChildElement('getDispatchedOrdersResponse','http://warehouse.docdata.co.uk/');
        List<dom.XmlNode> listSOItem = dispatchedOrdersResponse.getChildElements(); 
        List<DispatchedOrdersResponseItem> pList = new List<DispatchedOrdersResponseItem>();
        DispatchedOrdersResponseItem item;
        for(dom.XmlNode node : listSOItem) 
        {           
            item = new DispatchedOrdersResponseItem(); 
            item.OrderRef = node.getchildelement('orderRef',null) !=null? node.getchildelement('orderRef',null).gettext(): '';            
            item.Position = node.getchildelement('Position',null) !=null? integer.valueOf(node.getchildelement('Position',null).gettext()): null;            
            item.Status = node.getchildelement('status',null) !=null? node.getchildelement('status',null).gettext(): '';            
            item.ForHoleOrder = node.getchildelement('ForHoleOrder',null) !=null? boolean.valueOf(node.getchildelement('ForHoleOrder',null).gettext()): null;            
            item.ShippingDate = node.getchildelement('shippingDate',null) !=null? date.valueof(node.getchildelement('shippingDate',null).gettext()): null;            
            pList.add(item);
        }
        returnObject.positionList=pList;
        system.debug(returnObject);
        return returnObject;
    }
    
    public class DispatchedOrdersResponseItem
    {
        public String OrderRef{set;get;}
        public Integer Position{set;get;}
        Public String Status{set;get;}
        Public Boolean ForHoleOrder{set;get;}
        Public Date ShippingDate{set;get;}
        public DispatchedOrdersResponseItem(){}
    }   
}