@isTest(seealldata = true)
public class TestDocDataReturnRestService {

    static testMethod void testReturn() {
        //set up test data
        // PBSI__PBSI_Sales_Order__c before insert
        // 
        // create account
        // create contact
        // create opportunity
        // create sales order (opportunity.PBSI__Opportunity__c)
        // 
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = system.today();
        testOpportunity.StageName = 'Proposal/Price Quote';
        testOpportunity.Estimated_Dispatch_Date__c = system.today();
        testOpportunity.Shipper__c = 'hello world';
        testOpportunity.Wired_For__c = 'UK';
        testOpportunity.Packing__c = 'Individually boxed';
        testOpportunity.Customer_Order_Number__c = 'Customer_Order_Number__c';
        testOpportunity.Required_For__c = system.today();
        testOpportunity.Description = 'description';
        testOpportunity.Bespoke__c = true;
        testOpportunity.Urgent__c = true;
        testOpportunity.Call_Email_Before_Dispatch__c = true;
        testOpportunity.Sooner_Delivery_If_Possible__c = true;
        testOpportunity.Samples__c = true;
        testOpportunity.Allow_Return__c = true;
        testOpportunity.Combine_with_Order__c = 'combine with order';
        testOpportunity.Dispatch_Method__c = 'Customer pick-up';
        insert testOpportunity;
        
        PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        salesOrder.PBSI__Customer__c = testAccount.Id;
        salesOrder.PBSI__Opportunity__c = testOpportunity.Id;
        salesOrder.PBSI__Order_Date__c = system.today();
        salesOrder.PBSI__Due_Date__c = system.today() + 15;
        insert salesOrder;
        
        PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c limit 1];
        
        DocDataReturnRestService.RMALine line1 = new DocDataReturnRestService.RMALine();
        line1.Condition = 'Damaged';
        line1.SKU = item.name;
        line1.qtyReturned = 3;
        line1.ReasonCode = 'Damaged';
        
        List<DocDataReturnRestService.RMALine> lines = new List<DocDataReturnRestService.RMALine>();
        lines.add(line1);
        
        DocDataReturnRestService.RMA rma = new DocDataReturnRestService.RMA();
        rma.OrderId = salesOrder.Id;
        rma.Items = lines;
        rma.ReturnType = 'Return';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        DocDataReturnRestService.ReturnRequest(rma);

        
    }
}