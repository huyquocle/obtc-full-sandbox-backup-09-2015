/**
 *  @author: Paul Harvie (Income Systems)
 *  @date: 2014-09-12  
 *  @description: Email Helper Class
	  
 */



global class EmailUtils {

 global static void sendEmail(Id templateid,  ID WhatID, ID TargetObjectId, ID orgwideemailid) { 
        //if(recipients == null) return;
        //if(recipients.size() == 0) return;
        // Create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        //the email is not saved as an activity.
        //mail.setSaveAsActivity(false);
        // Assign the addresses for the To lists to the mail object.
        
        
        //mail.setToAddresses(recipients);          
        // Specify the subject line for your email address.
        
        //mail.setSubject(emailSubject);
        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(false);
        //if (useHTML) {
            // Specify the html content of the email.
        //mail.setHtmlBody(body);
        
        mail.setTemplateId(templateid);
        
        mail.setTargetObjectId(TargetObjectId);
        
        mail.setWhatId(WhatID);
        
        mail.setSaveAsActivity(true);
        
        
        mail.setOrgWideEmailAddressId(orgwideemailid);
        
        //mail.isUserMail()
        //mail.setPlainTextBody('this is a test');
        
           
        
    
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        //string bodystring = mail.getPlainTextBody();
        
        //System.debug('bodystring' + bodystring);
           //Select t.WhoId, t.WhatId, t.Subject, t.Status, t.Id, t.Description, t.CallType, t.CallObject, t.ActivityDate, t.AccountId From Task t where t.createddate = today
   
        //Task t = new Task(Subject = (mail.getSubject() + ' - copy - Just Product Info' ), Description = tabletext, WhatId = WhatID, WhoId = TargetObjectId ,  Status = 'Completed');
        //insert t;
        
        //System.debug('t' + t);
        
        
    }


}