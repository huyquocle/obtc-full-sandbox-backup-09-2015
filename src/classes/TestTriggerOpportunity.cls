@istest
public class TestTriggerOpportunity {
    public static testMethod void testSalesOrder()
    {
        c2g__codaGeneralLedgerAccount__c legerAccount = new c2g__codaGeneralLedgerAccount__c();
        legerAccount.Name = 'test general';
        legerAccount.c2g__ReportingCode__c = '123';
        legerAccount.CurrencyIsoCode = 'EUR';
        legerAccount.c2g__Type__c = 'Balance Sheet';
        insert legerAccount;
        
        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();
        taxCode.Name = 'GB-O-ZRO';
        taxCode.c2g__Description__c = 'Description';
        taxCode.CurrencyIsoCode = 'EUR';
        taxCode.c2g__GeneralLedgerAccount__c = legerAccount.id;
        insert taxCode;
         Account  account = new Account ();
        account.Name = 'test account';
		insert account;
        
        Opportunity opp = new Opportunity ();
        opp.Name = 'test';
        opp.Account = account;
        opp.CloseDate = system.Date.today();
        opp.Shipping_and_Handling__c = 100;
        opp.VAT_Code__c = 'GB-O-ZRO';
        opp.StageName = 'Closed Won';
        
        try
        {
			insert opp;         
        }
        catch( System.Exception  er)
        {}
    }
}