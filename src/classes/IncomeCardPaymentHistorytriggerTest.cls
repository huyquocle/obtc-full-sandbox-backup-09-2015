/**
 *  @author: Paul Harvie (Income Systems)
 *  @date: 2014-10-12  
 *  @description: Test class for testing:
	 IncomeCard Payment History Trigger
	  
 */



@isTest (seeAllData=true) 
private class IncomeCardPaymentHistorytriggerTest {
    //public static SCMFFSetUpTest tc = new SCMFFSetUpTest();
    
    
    static testMethod void myUnitTest() {
               
       
        Account thisAcc = new Account();
            
        thisAcc.Description = 'Test customer site';
        thisAcc.FAX = '555-1234';
       
        thisAcc.Industry = null;
       
        thisAcc.Name = 'Test Customer Paul';
        thisAcc.NumberOfEmployees = null;
        thisAcc.Phone = null;
        thisAcc.Ownership = null;
        thisAcc.ParentId = null;
      
        thisAcc.TickerSymbol = null;
        thisAcc.Type = null;
        thisAcc.Website = null;
       

        thisAcc.BillingStreet = '123 Main St\nSuite 101\nc/0George';
        thisAcc.BillingCity = 'San Francisco';
        thisAcc.BillingState = 'CA';
        thisAcc.BillingPostalCode = '12345';
        thisAcc.BillingCountry = 'US';

        thisAcc.ShippingStreet = '123 Main St\nSuite 101\nc/0George';
        thisAcc.ShippingCity = 'San Francisco';
        thisAcc.ShippingState = 'CA';
        thisAcc.ShippingPostalCode = '12345';
        thisAcc.ShippingCountry = 'US';
        
        insert thisAcc;

        //Contact thisCon = tc.createTestCustomerContact(true, thisAcc);
        
        Contact thisCon = new Contact();
		
		thisCon.AccountId = thisAcc.id;
		thisCon.AssistantPhone = null;
		thisCon.AssistantName = null;
		thisCon.Birthdate = null;
		thisCon.Department = null;
		thisCon.Description = null;
		thisCon.Email = 'test@aol.com';
		thisCon.FAX = null;
		thisCon.FirstName = 'Test';
		thisCon.HomePhone = null;
		//thisCon.SCMC__Language__c = null;
		thisCon.MailingCity = null;
		thisCon.MailingCountry = null;
		thisCon.MailingState = null;
		thisCon.MailingStreet = null;
		thisCon.MailingPostalCode = null;
		thisCon.MobilePhone = null;
		thisCon.Phone = '9015888555';
		thisCon.OtherPhone = null;
		thisCon.Salutation = 'Mr.';
		
		thisCon.ReportsTo = null;
		
		thisCon.Title = null;
		thisCon.LastName = 'Lastname';
        
        insert thisCon;
        
               
        
        //Opportunity thisOpp = tc.createTestOpportunity(thisAcc, false);
        Opportunity thisOpp = new Opportunity(
      		Name = 'TestOpp1'
      		, StageName = 'New'
      		, CloseDate = system.today()
      		, AccountId = thisAcc.id);
        
        
        
        thisOpp.CurrencyIsoCode = 'GBP';
        thisOpp.Company_Name__c = 'Original BTC';
        insert thisOpp;
        
        thisOpp.Billing_Contact__c = thisCon.id;
        thisOpp.Web_Site_Order__c = true;
        update thisOpp;
        
        Product2 dummyProduct = new Product2();
        dummyProduct.Name = 'Test Product';
        insert dummyProduct;
        
        Pricebook2 orgPriceBook = [Select Id From Pricebook2 Where Name like '%Standard%' LIMIT 1];
        
        PricebookEntry dummyPricebookEntry = new PricebookEntry();
        dummyPricebookEntry.IsActive = true;
        dummyPricebookEntry.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry.Product2Id = dummyProduct.Id;
        dummyPricebookEntry.UnitPrice = 10000;
        dummyPricebookEntry.CurrencyIsoCode = 'GBP';
        insert dummyPricebookEntry;
        
        
        
        PricebookEntry dummyPricebookEntry2 = new PricebookEntry();
        dummyPricebookEntry2.IsActive = true;
        dummyPricebookEntry2.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry2.Product2Id = dummyProduct.Id;
        dummyPricebookEntry2.UnitPrice = 10000;
        dummyPricebookEntry2.CurrencyIsoCode = 'EUR';
        insert dummyPricebookEntry2;
        
        PricebookEntry dummyPricebookEntry3 = new PricebookEntry();
        dummyPricebookEntry3.IsActive = true;
        dummyPricebookEntry3.Pricebook2Id = orgPriceBook.Id;
        dummyPricebookEntry3.Product2Id = dummyProduct.Id;
        dummyPricebookEntry3.UnitPrice = 10000;
        dummyPricebookEntry3.CurrencyIsoCode = 'USD';
        
        insert dummyPricebookEntry3;
        
        
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = thisOpp.Id;
        //oli.CurrencyIsoCode = 'USD';
        oli.PricebookEntryId = dummyPricebookEntry.Id;
        oli.UnitPrice = 123.99;
        oli.Quantity = 1;
        insert oli;
        
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli2.OpportunityId = thisOpp.Id;
        //oli2.CurrencyIsoCode = 'GBP';
        oli2.PricebookEntryId = dummyPricebookEntry.Id;
        oli2.UnitPrice = 123.99;
        oli2.Quantity = 1;
        insert oli2;
        
        OpportunityLineItem oli3 = new OpportunityLineItem();
        oli3.OpportunityId = thisOpp.Id;
        //oli3.CurrencyIsoCode = 'EUR';
        oli3.PricebookEntryId = dummyPricebookEntry.Id;
        oli3.UnitPrice = 123.99;
        oli3.Quantity = 1;
        insert oli3;
        
        
        Income_Card_Payment__c thisCardPayment = new Income_Card_Payment__c( Sagepay_Transaction_ID__c = '2353145', Sagepay_Security_Key__c = '234123421', 
        SagePay_Vendor__c = '21341234', Payment_Status__c = 'Authorised', Payment_Status_Details__c = 'Authorised',  Immediate_Amount__c = 500, Opportunity__c = thisOpp.id);
               
        insert thisCardPayment;
        
        system.debug('############# thisCardPayment =' + thisCardPayment); 
        
        Opportunity thisOpp2 = new Opportunity(
      		Name = 'TestOpp2'
      		, StageName = 'New'
      		, CloseDate = system.today()
      		, AccountId = thisAcc.id);
        
                   
        //Opportunity thisOpp2 = tc.createTestOpportunity(thisAcc, false);
        thisOpp2.CurrencyIsoCode = 'EUR';
        thisOpp2.Company_Name__c = 'Original BTC France';
        insert thisOpp2;
        
        system.debug('############# thisOpp2 =' + thisOpp2); 
        
        
        OpportunityLineItem oli4 = new OpportunityLineItem();
        oli4.OpportunityId = thisOpp2.Id;
        //oli3.CurrencyIsoCode = 'EUR';
        oli4.PricebookEntryId = dummyPricebookEntry2.Id;
        oli4.UnitPrice = 123.99;
        oli4.Quantity = 1;
        insert oli4;
        
        
        Income_Card_Payment__c thisCardPayment2 = new Income_Card_Payment__c( Sagepay_Transaction_ID__c = '2353145', Sagepay_Security_Key__c = '234123421', 
        SagePay_Vendor__c = '21341234', Payment_Status__c = 'Authorised', Payment_Status_Details__c = 'Authorised',  Immediate_Amount__c = 500, Opportunity__c = thisOpp2.id);
               
        insert thisCardPayment2;
        system.debug('############# thisCardPayment2 =' + thisCardPayment2); 
        
        Opportunity thisOpp3 = new Opportunity(
      		Name = 'TestOpp3'
      		, StageName = 'New'
      		, CloseDate = system.today()
      		, AccountId = thisAcc.id);
        
        //Opportunity thisOpp3 = tc.createTestOpportunity(thisAcc, false);
        thisOpp3.CurrencyIsoCode = 'USD';
        thisOpp3.Company_Name__c = 'Original BTC';
        insert thisOpp3;
        
        system.debug('############# thisOpp3 =' + thisOpp3); 
        
        
        OpportunityLineItem oli5 = new OpportunityLineItem();
        oli5.OpportunityId = thisOpp3.Id;
        //oli3.CurrencyIsoCode = 'USD';
        oli5.PricebookEntryId = dummyPricebookEntry3.Id;
        oli5.UnitPrice = 123.99;
        oli5.Quantity = 1;
        insert oli5;
        
        Income_Card_Payment__c thisCardPayment3 = new Income_Card_Payment__c( Sagepay_Transaction_ID__c = '2353145', Sagepay_Security_Key__c = '234123421', 
        SagePay_Vendor__c = '21341234', Payment_Status__c = 'Authorised', Payment_Status_Details__c = 'Authorised',  Immediate_Amount__c = 500, Opportunity__c = thisOpp3.id);
               
        insert thisCardPayment3;
        system.debug('############# thisCardPayment3 =' + thisCardPayment3); 
 
        
        //thisOpp.CurrencyIsoCode = 'EUR';
        //update thisOpp;
        
        //Income_Card_Payment__c thisCardPayment3 = new Income_Card_Payment__c( Sagepay_Transaction_ID__c = '2353145', Sagepay_Security_Key__c = '234123421', 
        //SagePay_Vendor__c = '21341234', Payment_Status__c = 'Authorised', Payment_Status_Details__c = 'Authorised',  Immediate_Amount__c = 500, Opportunity__c = thisOpp.id);
               
        //insert thisCardPayment3;
        
        
        
        
        
        system.debug('############# now make a cc history'); 
        
        Income_Card_Payment_History__c thishistory = new Income_Card_Payment_History__c (Transaction_Type__c = 'Sale', Payment_Status__c  = 'Authorised' 
        , Amount__c = 10, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory;
        system.debug('############# thishistory =' + thishistory); 
    
        //thisOpp.Company_Name__c = 'Original BTC France';
        //update thisOpp;
    
    
        Income_Card_Payment_History__c thishistory2 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Sale', Payment_Status__c  = 'Authorised' 
        , Amount__c = 10, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory2;
        system.debug('############# thishistory2 =' + thishistory2); 
        
        
        Income_Card_Payment_History__c thishistory3 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Refund', Payment_Status__c  = 'Authorised' 
        , Amount__c = 5, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory3;
        system.debug('############# thishistory3 =' + thishistory3); 
        
        Income_Card_Payment_History__c thishistory4 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Void', Payment_Status__c  = 'Authorised' 
        , Amount__c = 5, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory4;
        system.debug('############# thishistory4 =' + thishistory4); 
        
        
        thisOpp.Company_Name__c = 'The Lighting Library';
        update thisOpp;
    
        Income_Card_Payment_History__c thishistory5 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Sale', Payment_Status__c  = 'Authorised' 
        , Amount__c = 0, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory5;
        system.debug('############# thishistory5 =' + thishistory5); 
    
   
        
        thisOpp.Company_Name__c = 'LM England';
        update thisOpp;
    
        Income_Card_Payment_History__c thishistory6 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Sale', Payment_Status__c  = 'Authorised' 
        , Amount__c = 0, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory6;
        system.debug('############# thishistory6 =' + thishistory6); 
    
        
        //thisOpp.Company_Name__c = 'LM France';
        //update thisOpp;
        
          
        Income_Card_Payment_History__c thishistory7 = new Income_Card_Payment_History__c (Transaction_Type__c = 'Sale', Payment_Status__c  = 'Authorised' 
        , Amount__c = 0, Income_Card_Payment__c = thisCardPayment.id );
        insert thishistory7;
        system.debug('############# thishistory7 =' + thishistory7); 
    
    }
    
}