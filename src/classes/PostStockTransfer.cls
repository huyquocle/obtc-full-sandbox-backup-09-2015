public class PostStockTransfer {

    public string PO { get; set; }
    public string SKU { get; set; }
    public integer Quantity { get; set; }
    public string DateOfDelivery { get; set; }
    public string Comment { get; set; }
}