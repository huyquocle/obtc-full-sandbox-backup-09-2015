public class Constant {

    //Method name
    public static final string GET_NEW_TOKEN = 'GetNewToken';
    public static final string POST_NEW_PRODUCTS = 'PostNewProducts';
    public static final string POST_NEW_ORDERS = 'PostNewOrders';
    public static final string POST_STOCK_TRANSFER = 'PostStocksTransfer';
    
    //Request type
    public static final string POST = 'POST';
    public static final string GET = 'GET';
    
    //User name and password
    public static final string USERID = 'salesforce';
    public static final string PASSWORD = '3cr0fs3l@s';
    
    //Message error
    public static final string INVALID_TOKEN = 'Invalid token';
}