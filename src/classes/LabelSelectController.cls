public with sharing class LabelSelectController {

    public List<Document> documents {get;set;}
    public List<DocumentWrapper> documentWrappers {get;set;}
    public String selectedFolder {get;set;}
    public PBSI__PBSI_Item__c currentItem {get;set;}
    public String jsParam {get;set;}
    
    public LabelSelectController() {
        Id itemId = ApexPages.currentPage().getParameters().get('itd');
        currentItem = [SELECT Id,Name,PBSI__Photo_URL__c FROM PBSI__PBSI_Item__c WHERE Id = :itemId];
        // select all folder where type = document
        initFolderPicklist();
        // select document where folder id belong to above
        retriveDocumentByFolder();
    }
    
    public PageReference changeLabel() {
        currentItem.PBSI__Photo_URL__c = '/servlet/servlet.FileDownload?file=' + jsParam;
        update currentItem;
        return new PageReference('/' + currentItem.Id);
    }
    
    public void retriveDocumentByFolder() {
        //documents = new List<Document>();
        //documents = [SELECT Id,Name FROM Document WHERE FolderId = :selectedFolder];
        documentWrappers = new List<DocumentWrapper>();
        for(Document d: [SELECT Id,Name FROM Document WHERE FolderId = :selectedFolder AND (Type = 'jpg' OR Type = 'jpeg' OR Type = 'png') ORDER BY Name]) {
            documentWrappers.add( new DocumentWrapper(d) );
        }
    }
    
    public List<SelectOption> folderPicklist {get;set;}
    public void initFolderPicklist() { 
        folderPicklist = new List<SelectOption>();        
        List<Folder> folders = [SELECT ID,Name FROM Folder WHERE Type='Document'];
        selectedFolder = folders.get(0).Id;
        for(Folder f: folders) {
            folderPicklist.add( new SelectOption(f.Id, f.Name) );
        }
    }
    
    class DocumentWrapper {
        public Document doc {get;set;}
        public String imageUrl {get;set;}
        public DocumentWrapper(Document doc) {
            this.doc = doc;
            //
            initUrl();
        }
        private void initUrl() {
            // String strOrgId = UserInfo.getOrganizationId();
            // imageUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/servlet/servlet.ImageServer?id=' + doc.Id + '&oid=' + strOrgId;
            imageUrl = '/servlet/servlet.FileDownload?file=' + doc.Id;
        }
    }
}