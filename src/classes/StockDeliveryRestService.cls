/*
*       Changes:
*           - <sonny.le@huntermacdonald.com> 
*               20/08/2015 Added PO in the SOQL condition
*                   
*                   
*
*/

@RestResource(urlMapping='/StockDelivery/*')
global class StockDeliveryRestService {
    
    private static final string SUCCESS = 'Success';
    private static final string FAIL = 'Fail';
    
    private static final string ITEM_INVALID = 'Invalid item';
    private static final string ITEM_DELIVERED = 'Item has been delivered';
    
    @HttpPost
    global static void ItemUpdate (ItemRequest Item)
    {
        string status = '';
        string message = '';
        
        List<Stock_Delivery_History__c> deliveryHistory =  [select id, SKU__c, Comment__c, Date_of_Receive__c,
                                                            Receive_Quantity__c, Stock_Delivery_Status__c
                                                            FROM Stock_Delivery_History__c 
                                                            WHERE Item__r.name = :Item.SKU and Date_of_Receive__c = null    
                                                            AND PO__c = :Item.PO                                                         
                                                            order by createdDate ASC limit 1];                                                            
        
        if(deliveryHistory != null && !deliveryHistory.isEmpty()) {
            Stock_Delivery_History__c history = deliveryHistory[0];
            
            try {
                if(Item.DateOfReceive != null) {
                    history.Date_of_Receive__c = Item.DateOfReceive;
                } else {
                    history.Date_of_Receive__c = system.now();
                }
                
                history.Receive_Quantity__c = Item.Quantity;
                if(!String.isBlank(Item.Comment)) {
                    history.Comment__c = Item.Comment;
                }
                if(!String.isBlank(Item.PO)) {
                    //history.PO__c = Item.PO; // We don't want to overwrite the PO (dd/mm/yyyy format)
                }
                
                history.Stock_Delivery_Status__c = 'Delivered';                

                // This has been moved to a FLOW -- JE
                // Move the receive quantiy from Transfer to DocData location into DocData location.
                // If the received quantiy > quantity in TTDL, we flag up the History record. 
                // Need the Lot, PBSI__Item__r = Item.SKU and name = 'TTDL' ==> total quanity in this location
                // if (received quantity > total quanity) then use the total quanity in the lot, and flag up Stock Delivery history record. 
                // else received quantity use
                // Rename received quantity to received quantity in docdata call, create a new field called
                // PBSI.AscentAPI.moveItem(lstItem[0].PBSI__Item__r.Id, fromLotId, toLotId, toLocation, adjustQuantity, null);
                //history.new field = transferQuantity;

                update history;

                
                status = SUCCESS;
            } catch(Exception ex) {
                status = FAIL;
            }
                        
        } else {
            status = SUCCESS;
            message = ITEM_DELIVERED;
        }     
        string retXML = '<?xml version="1.0" encoding="UTF-8"?>';   
        retXML += '<response>';
        retXML += '<status>'+status+'</status>';
        retXML += '<message>' + message + '</message>';
        retXML += '</response>';
        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        RestContext.response.addHeader('Accept', 'application/xml');
        RestContext.response.responseBody = Blob.valueOf(retXML);
        
    }
    
    global class ItemRequest
    {
        global string 	PO			{get;set;}
        global string   SKU         {get;set;}
        global double   Quantity    {get;set;}
        global string   Comment		{get;set;}
        global DateTime DateOfReceive	{get;set;}
        global string   Warehouse   {set;get;}
    }
}