@RestResource(urlMapping='/Return/*')
global class DocDataReturnRestService {
    
    @HttpPost
    global static void ReturnRequest (RMA Returns)
    {
        string status = '';
        string message = '';
        //get sales order
        List<PBSI__PBSI_Sales_Order__c> so = [select id from PBSI__PBSI_Sales_Order__c where name = :Returns.OrderId limit 1];
        //Create new RMA
        Case rma = new Case();
        rma.Subject = 'Returned from DocData ' + Returns.ReturnType;
        rma.Status = 'New';
        rma.Reason = 'Customer Services';
        rma.PBSI__Return_Type__c = 'From Customer';
        rma.Status = 'Closed';
        rma.Origin = 'DocData';
        rma.PBSI__Return_Type__c = Returns.ReturnType;
        if(so != null && !so.isEmpty()) {
            rma.PBSI__Sales_Order__c = so[0].Id;
        }
        Savepoint sp = Database.setSavepoint();
        try {
            
            insert rma;
            //After creating rma successfully
            //create rma lines
            List<PBSI__RMA_Lines__c> lstLine = new List<PBSI__RMA_Lines__c>();
            for(RMALine line : Returns.Items) {
                PBSI__RMA_Lines__c rmaLine = new PBSI__RMA_Lines__c();
                rmaLine.PBSI__RMA__c = rma.Id;
                if(line.Condition.equals('Damaged')) {
                    rmaLine.PBSI__Condition__c = 'Damaged';
                }
                PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c where name = : line.SKU];
                rmaLine.PBSI__Item__c = item.id;
                if(!String.isBlank(line.Carrier)) {
                    rmaLine.Carrier__c = line.Carrier;
                }
                rmaLine.Return_origin__c = 'DocData originating';
                rmaLine.PBSI__Quantity__c = line.qtyReturned;
                rmaline.PBSI__Reason_RMA_Line__c = line.ReasonCode;
                //add reason tra
                lstLine.add(rmaLine);
            }
            insert lstLine;
            
            //create RMA Archived Line(process rma lines)
            List<PBSI__RMA_Line_Archive__c> archiveLines = new List<PBSI__RMA_Line_Archive__c>();
            
            for(PBSI__RMA_Lines__c l : lstLine) {
                PBSI__RMA_Line_Archive__c archive = new PBSI__RMA_Line_Archive__c();
                archive.PBSI__RMA__c = rma.Id;
                archive.PBSI__RMA_Line__c = l.Id;
                archive.PBSI__Sales_Order__c = l.PBSI__Sales_Order__c;
                archive.PBSI__Item__c = l.PBSI__Item__c;
                archive.PBSI__Quantity__c = Double.valueOf(l.PBSI__Quantity__c);
                archiveLines.add(archive);            
            }

            insert archiveLines;
            
            Case c = [select id, CaseNumber from Case where id = :rma.Id];
            //create new Movement journal
            PBSI__Movement_Journal__c mj = new PBSI__Movement_Journal__c();
            mj.PBSI__Type__c = 'RMA Returned from Customer';
            mj.PBSI__Reference_Type__c = 'RMA';
            mj.PBSI__Reference_Number__c = c.CaseNumber;
            mj.PBSI__Movement_Type__c = 202;
            insert mj;
            //location
            PBSI__PBSI_Location__c docReturnLocation = [select id, name from PBSI__PBSI_Location__c where name='DocData Returns'];
            
            for(PBSI__RMA_Line_Archive__c archive : archiveLines) {
                List<PBSI__Lot__c> docdata = [select id, name from PBSI__Lot__c where name = 'DocData' and PBSI__Item__c = :archive.PBSI__Item__c limit 1 ];
                List<PBSI__Lot__c> docdataReturn = [select id, name from PBSI__Lot__c where name = 'DocData Returns'  and PBSI__Item__c = :archive.PBSI__Item__c limit 1];                        
                //create movement line
                
                PBSI__Movement_Line__c ml = new PBSI__Movement_Line__c();
                ml.PBSI__Movement_Journal__c = mj.Id;
                ml.PBSI__Item__c = archive.PBSI__Item__c;
                ml.PBSI__Transfer_Quantity__c = Double.valueOf(archive.PBSI__Quantity__c);
                ml.PBSI__RMA_Line__c = archive.PBSI__RMA_Line__c;
                /*if(docdata != null && !docdata.isEmpty()) {
                    ml.PBSI__Lot__c = docdata[0].id;
                }*/
                if(docdataReturn == null || docdataReturn.isEmpty()) {
                    PBSI__Lot__c docReturn = new PBSI__Lot__c();
                    docReturn.PBSI__Item__c = archive.PBSI__Item__c;
                    docReturn.PBSI__Location__c = docReturnLocation.id;
                    docReturn.Name = docReturnLocation.Name;
                    insert docReturn;
                    ml.PBSI__To_Lot__c = docReturn.id;
                } else {
                    ml.PBSI__To_Lot__c = docdataReturn[0].id;
                }
                insert ml;
            }
            status = 'Success';
        }catch(Exception e) {
            system.debug('Cause: '+e.getCause() + 'Message: ' + e.getMessage());
            status = 'Fail';
            message = 'Can not create RMA';
            Database.rollback(sp);
        }
        
        string retXML = '<?xml version="1.0" encoding="UTF-8"?>';   
        retXML += '<response>';
        retXML += '<status>'+status+'</status>';
        retXML += '<message>' + message + '</message>';
        retXML += '</response>';
        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        RestContext.response.addHeader('Accept', 'application/xml');
        
        RestContext.response.responseBody = Blob.valueOf(retXML);
    }
    
    global class RMA {
        global String OrderId {get;set;}
        global List<RMALine> Items {get;set;}
         global string ReturnType{get;set;}
    }
    
    global class RMALine
    {
        global string Carrier {get;set;}
        global string Condition {get;set;}
        global string SKU {get;set;}    
        global integer qtyReturned {get;set;}
        global string ReasonCode {get;set;}
        
        
        
    }        
}