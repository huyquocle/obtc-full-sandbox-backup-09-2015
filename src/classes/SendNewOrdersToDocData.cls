/*
*       Changes:
*           - <sonny.le@huntermacdonald.com> 
*               20/08/2015 Added a 15 mins delay: JIRA ODI-43
*                   
*                   
*
*/

global class SendNewOrdersToDocData implements  Database.Batchable<sObject>, Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext info){ 

        Datetime d = Datetime.now().addMinutes(-15);
        if (Test.IsRunningTest()) d = Datetime.now();
        
        return  Database.getQueryLocator([select id,Name,
                                          PBSI__Delivery_Company__c,
                                          PBSI__Delivery_Streetnew__c,
                                          Shipping_First_Name__c,
                                          Shipping_Last_Name__c,
                                          PBSI__Delivery_Address_line2__c,
                                          PBSI__Delivery_City__c,
                                          PBSI__Delivery_Postal_Code__c,
                                          PBSI__Delivery_Country__c,
                                          PBSI__Contact_Email__c,
                                          Shipping_Telephone__c,
                                          PBSI__Order_Date__c,
                                          Dispatch_Method__c,
                                          PBSI__Final_Order_Total__c,
                                          Final_Order_Total_Inc_Tax__c,
                                          (SELECT id,Name,PBSI__Quantity__c,PBSI__Price__c,PBSI__Total_Price__c,
                                          PBSI__Quantity_Needed__c,PBSI__Item__c,PBSI__Item__r.Name 
                                                              FROM PBSI__Sales_Order_Lines__r where PBSI__Item__r.Name !='SH000' )
                                          FROM PBSI__PBSI_Sales_Order__c where Sent_to_DC__c  = false                                           
                                          and Fulfilment__c='Fulfillment' AND createdDate < :d
                                         ]);        
    }     
    global void execute(Database.BatchableContext info, List<PBSI__PBSI_Sales_Order__c> soList){
        
        List<PostNewOrder> posList = new List<PostNewOrder>();
        List<PostNewOrder.OrderLine> orderLineTmpList = new List<PostNewOrder.OrderLine>();
        set<id> ids = new set<id>();
        List<PBSI__PBSI_Sales_Order_Line__c> allLineItems = new List<PBSI__PBSI_Sales_Order_Line__c>();
        for(PBSI__PBSI_Sales_Order__c orderItem:soList)
        {
            //List Sales order lines
            List<PBSI__PBSI_Sales_Order_Line__c> sol = orderItem.PBSI__Sales_Order_Lines__r;
            if(sol != null && !sol.isEmpty()) {
                
                
                ids.add(orderItem.Id);
                orderLineTmpList = new List<PostNewOrder.OrderLine>();
                PostNewOrder item = new PostNewOrder();
                item.OrderId = orderItem.Name;         
                item.Company= orderItem.PBSI__Delivery_Company__c;
                item.FirstName= orderItem.Shipping_First_Name__c;
                item.LastName= orderItem.Shipping_Last_Name__c;
                item.AddressLine1= orderItem.PBSI__Delivery_Streetnew__c;
                item.AddressLine2= orderItem.PBSI__Delivery_Address_line2__c;
                item.City= orderItem.PBSI__Delivery_City__c;
                item.Postcode= orderItem.PBSI__Delivery_Postal_Code__c;
                item.Country= orderItem.PBSI__Delivery_Country__c;
                item.Email= orderItem.PBSI__Contact_Email__c;
                item.Phone= orderItem.Shipping_Telephone__c;
                item.Fax= '';
                item.IsResidential= true;
                item.OrderDate= string.valueOf(orderItem.PBSI__Order_Date__c);
                item.ShipmentMethod= orderItem.Dispatch_Method__c;
                item.FinalOrderTotal = orderItem.PBSI__Final_Order_Total__c;
                item.FinalOrderTotalIncTax =orderItem.Final_Order_Total_Inc_Tax__c;
                
                for (PBSI__PBSI_Sales_Order_Line__c orderLine :sol)
                {
                    allLineItems.add(orderLine);
                    PostNewOrder.OrderLine lineTmpItem = new PostNewOrder.OrderLine();
                    lineTmpItem.SKU = orderLine.PBSI__Item__r.Name;
                    lineTmpItem.Quantity = integer.valueOf(orderLine.PBSI__Quantity_Needed__c);
                    lineTmpItem.SalePrice = orderLine.PBSI__Price__c;
                    orderLineTmpList.add(lineTmpItem);
                }
                item.OrderLines = orderLineTmpList;
                posList.add(item);
            }            
        }        
        List<String> returnIds = ODIWebServices.postOrders(posList); 
        system.debug('***** returnIds ' + returnIds);
        
        //Update lot
        if(allLineItems != null && allLineItems.size() > 0)
        {
            set<id> itemId = new set<id>();
            for (PBSI__PBSI_Sales_Order_Line__c orderLine :allLineItems)
            {               
                if(!itemId.contains(orderLine.PBSI__Item__c))
                {
                    itemId.add(orderLine.PBSI__Item__c);
                }   
                
            }
            List<PBSI__Lot__c> docdata = [select id, name,PBSI__Item__c from PBSI__Lot__c where name = 'DocData' and PBSI__Item__c = :itemId ];
            Map<id,id> itemdocId = new Map<id,id>();
            for(PBSI__Lot__c item:docdata)
            {
                if(itemdocId.get(item.PBSI__Item__c)==null)
                {
                    itemdocId.put(item.PBSI__Item__c,item.id);
                }
            }
            for (PBSI__PBSI_Sales_Order_Line__c orderLine :allLineItems)
            {
                orderLine.PBSI__Lot__c = itemdocId.get(orderLine.PBSI__Item__c);
            }
            //update allLineItems;            
        }
        
        
        Set<String> OrderName = new Set<String>(returnIds);
        List<PBSI__PBSI_Sales_Order__c> updateList = [select id,Name,Sent_To_DC__c,PBSI__Status__c from PBSI__PBSI_Sales_Order__c
                                                      where id in:ids];
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        if(updateList != null && updateList.size() > 0)
        {
            for(PBSI__PBSI_Sales_Order__c item:updateList )
            {
                system.debug('**** OrderName: ' + OrderName);
                system.debug('**** item.Name: ' + item.Name);                
                system.debug('**** || ');                

                if(OrderName.contains(item.Name)) {
                    item.Sent_To_DC__c = true;
                    item.PBSI__Status__c = 'Pending DocData acceptance';
                }
            }

            system.debug('***** updateList: ' + updateList);

            update updateList;    
        }
        
        //Get sales order to create invoice
        updateList = [ select id,Name,Invoice_Generated__c
                      FROM PBSI__PBSI_Sales_Order__c 
                      WHERE id not in (select Ascent2FF__Sales_Order__c from c2g__codaInvoice__c  ) 
                      AND Invoice_Generated__c != True and id in:ids];
        set<string> soName = new set<string>();
        Map<String,PBSI__PBSI_Sales_Order__c> soMap = new Map<String,PBSI__PBSI_Sales_Order__c>();
        for(PBSI__PBSI_Sales_Order__c item :updateList){
            if(!soName.contains(item.name)){
                soName.add(item.name);
                soMap.put(item.name, item);
            }
        }
        List<c2g__codaInvoice__c> coda = [select id ,c2g__CustomerReference__c
                                          FROM c2g__codaInvoice__c 
                                          WHERE c2g__CustomerReference__c in:soName];
        try
        {
            for(c2g__codaInvoice__c codaIn:coda){
                if(soMap.get(codaIn.c2g__CustomerReference__c) != null){
                    soMap.remove(codaIn.c2g__CustomerReference__c);
                }
            }    
        }
        catch(Exception e)
        {
            
        }
        
        
        if(!soMap.isEmpty() & soMap.size() > 0) {
            try{
                //TriggerUtiSO.CreateFFInvoice(soMap.values());
            }catch(Exception e) {
                system.debug(e.getMessage());
            }            
        }
    }     
    global void finish(Database.BatchableContext info){     
    } 
}