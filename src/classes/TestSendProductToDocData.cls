@isTest(seealldata = true)
global class TestSendProductToDocData implements HttpCalloutMock{

    private HttpResponse resp;
    public TestSendProductToDocData(String testResponse) {
        resp = new HttpResponse();
        resp.setBody(testResponse);
        resp.setStatusCode(200);
        resp.setStatus('OK');
    }
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static HttpResponse doInvoke() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://api.salesforce.com/foo/bar?id=');
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        return res;
    }
    
    global static testMethod void sendToDocData() {
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
        insert itemGroup;
    
        PBSI__PBSI_Location__c loc=new PBSI__PBSI_Location__c(Name='DocData Location');  
        insert loc;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Default_Location__c  = loc.ID;
        item.PBSI__Item_Group__c        = itemGroup.ID;
        item.PBSI__description__c = 'description';
        item.Name = 'SKU001';
		item.Is_Fulfilled__c = true;
        item.Ascent4Prods__Do_Not_Sync__c = false;
        insert item;
        
        Test.startTest();
        
       	String testResponse = '{"Total":1,"Skus":["SKU001"]}';
       	HttpCalloutMock mock = new TestSendProductToDocData(testResponse);
       	Test.setMock(HttpCalloutMock.class, mock);
        HttpResponse res = doInvoke();
    }
}