@RestResource(urlMapping='/OrderStatus/*')
global class DocDataOrderStatusRestService {
    
    private static final string SUCCESS = 'Success';
    private static final string FAIL = 'Fail';
    private static final string INVALID_SO = 'Invalid Sales Order';
    private static final string INVALID_MESSAGE = 'Message incorrect';
    
    // accept XML a list of Order Lists
    @HttpPost
    global static void OrderStatus (OrderList Order)
    {
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        string status = '';
        string message = '';
        //Logic to get the oderlists and update status
        List<PBSI__PBSI_Sales_Order__c> SOList =  [select id, name, PBSI__Status__c,
                                                   (select id, PBSI__Item__r.id, PBSI__Item__r.name from PBSI__Sales_Order_Lines__r)
                                                   FROM PBSI__PBSI_Sales_Order__c 
                                                   WHERE name = :Order.OrderId and Sent_to_DC__c = true limit 1];
        
        
        
        if(SOList != null && SOList.size()> 0)
        {
            boolean isUpdateSO = false;
            //Update SO
            PBSI__PBSI_Sales_Order__c so = SOList[0];
            
            if(!String.isBlank(Order.Status)) {
                string stt = order.status.trim().toUpperCase();
                if(stt.contains('PROGRESS'))
                {
                    so.PBSI__Status__c = 'In progress at DocData';
                }   
                else if(stt == 'CANCEL' || stt == 'CANCELLED')
                {
                    so.PBSI__Status__c = 'Cancelled';
                }
                /*if(Order.Status.equals('Production complete'))
                {
                    PBSI__Tracking_Code__c
                    so.PBSI__Stage__c = 'Dispatch Complete'; 
                    so.PBSI__Status__c = 'Production complete'; 
                }*/
                isUpdateSO = true;
            } 
            if(isUpdateSO) {
                try {
                    update so;
                    status = SUCCESS;
                }catch(Exception e) {
                    system.debug(e.getMessage());
                    status = FAIL;
                    message = INVALID_MESSAGE;
                }
            }
            
            if(!String.isBlank(Order.Status) && (Order.Status.trim().toUpperCase().contains('DESPATCH COMPLETE'))) {
                List<PBSI__PBSI_Sales_Order_Line__c> SH000 = [select id, name, PBSI__Line_Status__c, PBSI__Item__r.name,PBSI__Sales_Order__c,PBSI__Quantity__c,PBSI__Item__c
                                                              from PBSI__PBSI_Sales_Order_Line__c 
                                                              where PBSI__Sales_Order__r.name = :Order.OrderId AND PBSI__Item__r.name = 'SH000' limit 1];

                
                Map<String, PBSI__PBSI_Sales_Order_Line__c> mapSOL = new Map<String, PBSI__PBSI_Sales_Order_Line__c>();
                
                List<PBSI__PBSI_Sales_Order_Line__c> soLineList = SOList[0].PBSI__Sales_Order_Lines__r;
                for(PBSI__PBSI_Sales_Order_Line__c sol : soLineList) 
                {
                    mapSOL.put(sol.PBSI__Item__r.name, sol);
                }
                
                system.debug('#SH000#' + SH000);
                
                system.debug('#mapSOL#' + mapSOL);
                PBSI__Shipped_Sales_Order_Line__c shipped;
                List<PBSI__Shipped_Sales_Order_Line__c> shippedSO = new List<PBSI__Shipped_Sales_Order_Line__c>();
                if(SH000 != null && SH000.size() > 0) {
                    if(SH000[0].PBSI__Line_Status__c != 'Packed') {
                        List<PBSI__Lot__c> lstLot = [select id from PBSI__Lot__c where PBSI__Item__r.Name = :SH000[0].name];
                        shipped = new PBSI__Shipped_Sales_Order_Line__c();                
                        shipped.PBSI__Sales_Order__c = SH000[0].PBSI__Sales_Order__c;               
                        shipped.PBSI__Quantity_Shipped__c = 1;
                        if(lstLot != null && !lstLot.isEmpty()) {
                            shipped.PBSI__Lot__c = lstLot[0].id; 
                        }                               
   
                            shipped.PBSI__Item__c = SH000[0].PBSI__Item__c;
                            shipped.PBSI__Sales_Order_Line__c = SH000[0].id; 
                        shippedSO.add(shipped);
                    }
                }
                
                
                for(Item sItem:Order.Items)
                {
                    system.debug('#Item#' + sItem.SKU);
                    List<PBSI__Lot__c> lstLot = [select id from PBSI__Lot__c where PBSI__Item__r.Name = :sItem.SKU AND name = 'DocData' limit 1];
                    PBSI__PBSI_Sales_Order_Line__c sol = mapSOL.get(sItem.SKU);
                    system.debug('#Lot#' + lstLot );
                    shipped = new PBSI__Shipped_Sales_Order_Line__c();                
                    shipped.PBSI__Sales_Order__c = SOList[0].id;               
                    shipped.PBSI__Quantity_Shipped__c = sItem.Quantity;
                    shipped.Tracking_Code__c = Order.TrackingCode;
                    if(lstLot != null && !lstLot.isEmpty()) {
                        shipped.PBSI__Lot__c = lstLot[0].id; 
                    }                               
                    if(sol != null)
                    {    
                        shipped.PBSI__Item__c = sol.PBSI__Item__c;
                        shipped.PBSI__Sales_Order_Line__c = sol.id; 
                    }
                    shippedSO.add(shipped);
                }  
                if(shippedSO.size() > 0)
                {
                    try
                    {
                        insert shippedSO;
                        //Re-Update SO
                       SOList =  [select id, name, PBSI__Status__c,PBSI__Stage__c
                                                   from PBSI__PBSI_Sales_Order__c
                                                   WHERE name = :Order.OrderId and Sent_to_DC__c = true limit 1];
                                                   
                       system.debug('#SOList#' + SOList );
                       if(SOList != null && SOList.size() > 0 )
                       {
                          if( SOList[0].PBSI__Status__c == 'Production Complete')
                          {
                              SOList[0].PBSI__Stage__c = 'Dispatch Complete';
                          }
                          else if(SOList[0].PBSI__Status__c == 'Partially complete')
                          {
                              SOList[0].PBSI__Stage__c = 'Partially dispatched';
                          }
                          update SOList;
                       }
                       
                        Set<id> shippedSOLineIds = (new Map<id, PBSI__Shipped_Sales_Order_Line__c>(
                            [select id from PBSI__Shipped_Sales_Order_Line__c where PBSI__Sales_Order__c = : SOList[0].id])).keySet();
                        system.debug('shippedSOLineIds: ' + shippedSOLineIds);
                        if(shippedSOLineIds != null && !shippedSOLineIds.isEmpty()) {                     
                            PBSI.AscentAPI.createSOMovementLines(shippedSOLineIds);
                        }
                        
                        
                        
                        status = SUCCESS ;//+ ' status =' + SOList[0].PBSI__Status__c;
                    }
                    catch (Exception e)
                    {
                        system.debug(e.getMessage());                        
                    }
                }
            }            
        }
        else
        {
            status = FAIL; 
            message = INVALID_SO;
        }        
        string retXML = '<?xml version="1.0" encoding="UTF-8"?>';   
        retXML += '<response>';
        retXML += '<status>'+status+'</status>';
        retXML += '<message>' + message + '</message>';
        retXML += '</response>';
        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        RestContext.response.addHeader('Accept', 'application/xml');
        
        RestContext.response.responseBody = Blob.valueOf(retXML);
    }
    
    global class OrderList
    {
        global string OrderId {get;set;}
        global string Status {get;set;}    
        global list<Item> Items {get;set;}  
        global string Warehouse {get;set;}
        global string TrackingCode{get;set;}
    }
    
    global class Item
    {
        global string   SKU         {get;set;}
        global integer  Quantity    {get;set;} 
    }
}