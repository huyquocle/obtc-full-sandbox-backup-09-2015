global class ODIWebServices {

    public static String token = '';
    
    
    public static String getNewToken(string userId, string password) {
        //get endpoint
        ODIWebservice__c odi = ODIWebservice__c.getInstance();
        string endpoint = odi.Webservice_Enpoint__c + Constant.GET_NEW_TOKEN + '?UserId=' + Constant.USERID + '&Password=' + Constant.PASSWORD;
                
        HttpResponse res = callWebService(null, endpoint, Constant.GET, null);
        if(res.getStatus().equals('OK') && res.getStatusCode() == 200) {
            token = res.getBody().substringBetween('"', '"');

        }
        return token;
    }
    
    
    /*
        Post list of new product to Docdata
    */
    @future(callout=true)
    public static void postProducts(String newProducts, String sentItems) {
        //response from Docdata when calling PostNewProducts API
        List<String> productSkus;
        
        List<PBSI__PBSI_Item__c> items = (List<PBSI__PBSI_Item__c>)Json.deserialize(sentItems, List<PBSI__PBSI_Item__c>.class);

        if(String.isBlank(token)) {
            getNewToken(Constant.USERID,Constant.PASSWORD);
        }
        
        //body
        String body = newProducts;
        
        //get endpoint
        ODIWebservice__c odi = ODIWebservice__c.getInstance();
        string endpoint = odi.Webservice_Enpoint__c + Constant.POST_NEW_PRODUCTS;       
        
        HttpResponse res = callWebService(token, endpoint, Constant.POST, body);
        system.debug('#res#'+res);
        system.debug('#response body#'+res.getBody());
        if(res.getStatus().equals('OK') && res.getStatusCode() == 200) {
            string response = res.getBody();
            productSkus = jsonProductToList(response);
            
            //List of items have been sent successfully
            List<PBSI__PBSI_Item__c> successItems = new List<PBSI__PBSI_Item__c>();
            
            Set<String> successIds = new Set<String>(productSkus);
            for(PBSI__PBSI_Item__c i : items) {
                if(successIds.contains(i.Name)) {
                    //update this item has been sent to Docdata
                    i.Post_to_DocData__c = 'Success';                
                } else {
                    i.Post_to_DocData__c = 'Fail';
                }
                i.Post_to_DocData_Date__c = system.now();
                successItems.add(i);
            }
            system.debug('#successItems#' + successItems);
            if(!successItems.isEmpty()) {
                update successItems;
            }
        }        
    }
    
    /*
        Post list of new order to Docdata
        Return list of OrderId
    */
    public static List<String> postOrders(List<PostNewOrder> newOrders) {
        List<String> orderId;
        if(String.isBlank(token)) {
            getNewToken(Constant.USERID,Constant.PASSWORD);
        }

        //body
        string body = JSON.serialize(newOrders);
        system.debug('New Order: ' + newOrders);
        system.debug('Body: ' + body);
        //get endpoint
        ODIWebservice__c odi = ODIWebservice__c.getInstance();
        string endpoint = odi.Webservice_Enpoint__c + Constant.POST_NEW_ORDERS;                

        HttpResponse res = callWebService(token, endpoint, Constant.POST, body);
        
        if(res.getStatus().equals('OK') && res.getStatusCode() == 200) {
            string response = res.getBody();
            orderId = jsonOrderToList(response);
            system.debug('#orderid#' + orderId);
            system.debug('#response#' + response);
        }        
        return orderId;
    }
    
    @future(callout=true)
    public static void postStockTransfer(String stock,Set<id> historyIds) {
    //public static void postStockTransfer(String stock) {
        List<String> skus;
        if(String.isBlank(token)) {
            getNewToken(Constant.USERID,Constant.PASSWORD);
        }

        //body
        string body = stock;
        system.debug('Transfer = ' + body);
        //get endpoint
        ODIWebservice__c odi = ODIWebservice__c.getInstance();
        string endpoint = odi.Webservice_Enpoint__c + Constant.POST_STOCK_TRANSFER;                

        HttpResponse res = callWebService(token, endpoint, Constant.POST, body);  
        List<Stock_Delivery_History__c> reUpdateList = [SELECT id,
                                                            Item__r.Name,
                                                            Transfer_Request_DD_Response__c,
                                                            Transfer_Request_Recieved_by_DD__c 
                                                            FROM Stock_Delivery_History__c where id in: historyIds]; 
                                                                
        if(res.getStatus().equals('OK') && res.getStatusCode() == 200) {
            string response = res.getBody();
            skus = jsonStockTransferToList(response);
          
            Set<string> skuset = new set<string>();
            for(String s:skus)
            {
                skuset.add(s);
            }
            for(Stock_Delivery_History__c hitem:reUpdateList)
            {
                hitem.Transfer_Request_Recieved_by_DD__c = system.now();
                if(skuset.contains(hitem.Item__r.Name))
                {                   
                    hitem.Transfer_Request_DD_Response__c = 'Success';                  
                }
                else
                {
                    hitem.Transfer_Request_DD_Response__c = 'Failed';   
                }
            }         
            
        }
        else
        {
            for(Stock_Delivery_History__c hitem:reUpdateList)
            {
                hitem.Transfer_Request_Recieved_by_DD__c = system.now();
                hitem.Transfer_Request_DD_Response__c = 'Failed';  
            }        
        }
        if(reUpdateList != null && reUpdateList.size() > 0)
        {
            update reUpdateList;
        }
               
        //return skus;
    }
    
    private static HttpResponse callWebService(String tokenAccess, String endpoint, String method, String body) {
        HttpRequest req = new HttpRequest();
        if(!String.isBlank(tokenAccess)) {
            req.setHeader('Token', tokenAccess);
        }
        if(!String.isBlank(body)) {
            req.setBody(body);
        }
        if(!String.isBlank(method)) {
            req.setMethod(method);
        }
        if(!String.isBlank(endpoint)) {
            req.setEndpoint(endpoint);
        }
        
        //req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Content-Type', 'application/json');

        Http http = new Http();
        return http.send(req);
    }
    
    /*
        Deserialize json response to JsonOrder object
        Put OrderId to a list
        Return the List
    */
    private static List<String> jsonOrderToList(String json) {
        //deserialize json to JsonOrder object
        try {
            JsonOrder jo = (JsonOrder)system.JSON.deserialize(json, JsonOrder.class);
            List<String> lstOrderId = new List<String>();
            
            for(Order o : jo.Orders) {
                if(o.Status.equals('I')) {
                    lstOrderId.add(o.OrderRef);
                }                
            }
            return lstOrderId;
        }catch(Exception e) {
            system.debug(e.getMessage());
            return null;
        }
    }
    
    
    /*
        Deserialize json response to JsonProduct object     
        Return list of Sku
    */
    private static List<String> jsonProductToList(String json) {
        try {
            JsonProduct jP = (JsonProduct)system.JSON.deserialize(json, JsonProduct.class);
            return jP.Skus;
        }catch(Exception e) {
            system.debug(e.getMessage());
            return null;
        }        
    }
    
    private static List<String> jsonStockTransferToList(String json) {
        try {
            JsonStockTransfer jP = (JsonStockTransfer)system.JSON.deserialize(json, JsonStockTransfer.class);
            return jP.Skus;
        }catch(Exception e) {
            system.debug(e.getMessage());
            return null;
        }   
    }
    
    //Classes match with the response from DocData
    private class JsonOrder {
        public integer Total{get;set;}
        public List<Order> Orders{get;set;}                
    }
    private class Order {
        public String OrderRef{get;set;}
        public String DbIndex{get;set;}
        public String Status {get;set;}
    }
    private class JsonProduct {
        public integer Total{get;set;}
        public List<String> Skus{get;set;}
    }
    private class JsonStockTransfer {
        public List<String> Skus{get;set;}
    }
}