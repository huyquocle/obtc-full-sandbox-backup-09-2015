@istest(seealldata = true)
global class TestSendMovementItemToDocData implements HttpCalloutMock{

    private HttpResponse resp;
    public TestSendMovementItemToDocData(String testResponse) {
        resp = new HttpResponse();
        resp.setBody(testResponse);
        resp.setStatusCode(200);
        resp.setStatus('OK');
    }
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static HttpResponse doInvoke() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://api.salesforce.com/foo/bar?id=');
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        return res;
    }
    
    global static testMethod void sendToDocData() {
        //set up test data
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
        insert itemGroup;
        
        PBSI__PBSI_Location__c location = [select id, name from PBSI__PBSI_Location__c where name = 'Default Location' limit 1];
        
        //PBSI__PBSI_Location__c docdata = [select id, name from PBSI__PBSI_Location__c where name = 'DocData' limit 1];
        List<PBSI__PBSI_Location__c> docdatas=[select id from PBSI__PBSI_Location__c where name = 'DocData'];
        PBSI__PBSI_Location__c docdata;
        if(docdatas == null || docdatas.size() == 0) {
            docdata = new PBSI__PBSI_Location__c(Name='DocData');  
        	insert docdata;
        } else {
            docdata = docdatas[0];
        }
  /*      
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Default_Location__c  = location.ID;
        item.PBSI__Item_Group__c        = itemGroup.ID;
        item.PBSI__description__c = 'description';
        item.Name = 'SKU001';
		item.Is_Fulfilled__c = false;
        item.Ascent4Prods__Do_Not_Sync__c = false;
        insert item;
    */
        PBSI__PBSI_Item__c item = [select id, name from PBSI__PBSI_Item__c limit 1];
        PBSI__Lot__c fromLot = [select id, name from PBSI__Lot__c where Name = 'Default Location' and PBSI__Item__c = :item.id limit 1];
        /*
        PBSI__Lot__c fromLot = new PBSI__Lot__c(Name='Default Location');
        fromLot.PBSI__Item__c = item.id;
        fromLot.PBSI__Location__c = location.id;
        insert fromLot;
        */
        PBSI__PBSI_Inventory__c inven = new PBSI__PBSI_Inventory__c();
        inven.PBSI__location_lookup__c = location.Id;
        inven.PBSI__item_lookup__c = item.Id;
        inven.PBSI__qty__c = 100;
        insert inven;
        
        List<PBSI__Movement_Journal__c> mjs = [select id, PBSI__Type__c from PBSI__Movement_Journal__c where PBSI__Type__c = 'Transfer Posting Location to Location'];
        PBSI__Movement_Journal__c mj;
        if(mjs == null || mjs.size() == 0) {
            mj = new PBSI__Movement_Journal__c();
        	mj.PBSI__Type__c = 'Transfer Posting Location to Location';
        	mj.PBSI__Movement_Type__c = 500;
        	insert mj;
        } else {
            mj = mjs[0];
        }
         
        PBSI__Movement_Line__c ml = new PBSI__Movement_Line__c();
        ml.PBSI__Item__c = item.Id;
        ml.PBSI__Transfer_Quantity__c = 6;
        ml.PBSI__From_Location__c = location.id;
        ml.PBSI__To_Location__c = docdata.Id;
        ml.PBSI__Transaction_Date__c = system.now();
        ml.PBSI__Movement_Journal__c = mj.id;
        ml.PBSI__Lot__c = fromLot.Id;
        insert ml;
        
        Test.startTest();
        
       	String testResponse = '{"Skus":["Sku 001"]}';
       	HttpCalloutMock mock = new TestSendMovementItemToDocData(testResponse);
       	Test.setMock(HttpCalloutMock.class, mock);
        HttpResponse res = doInvoke();
    }
}