public with sharing class PostNewProduct {

	public string Sku { get; set; }
    public string Barcode { get; set; }
    public string Description { get; set; }
    public string Supplier { get; set; }
    public string Category { get; set; }
    public string Height { get; set; }
    public string Width { get; set; }
    public string Depth { get; set; }
    public double Value { get; set; }
    public double RetailValue { get; set; }
}