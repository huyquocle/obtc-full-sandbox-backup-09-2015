@istest(SeeAllData=true)
public class TestTriggerPalletInformation {
    private static testMethod void testTrigger()
    {
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.CloseDate = system.today();
        testOpportunity.StageName = 'Proposal/Price Quote';
        testOpportunity.Estimated_Dispatch_Date__c = system.today();
        testOpportunity.Shipper__c = 'hello world';
        testOpportunity.Wired_For__c = 'UK';
        testOpportunity.Packing__c = 'Individually boxed';
        testOpportunity.Customer_Order_Number__c = 'Customer_Order_Number__c';
        testOpportunity.Required_For__c = system.today();
        testOpportunity.Description = 'description';
        testOpportunity.Bespoke__c = true;
        testOpportunity.Urgent__c = true;
        testOpportunity.Call_Email_Before_Dispatch__c = true;
        testOpportunity.Sooner_Delivery_If_Possible__c = true;
        testOpportunity.Samples__c = true;
        testOpportunity.Allow_Return__c = true;
        testOpportunity.Combine_with_Order__c = 'combine with order';
        testOpportunity.Dispatch_Method__c = 'Customer pick-up';
        insert testOpportunity;
    
        PBSI__PBSI_Sales_Order__c saleOrder = new PBSI__PBSI_Sales_Order__c();
        saleOrder.PBSI__Order_Date__c = Date.today();
        saleOrder.PBSI__Opportunity__c = testOpportunity.Id;
        insert saleOrder;
        
        Pallet_Information__c pallet = new Pallet_Information__c();
        pallet.Sales_Order__c = saleOrder.Id;
        pallet.CurrencyIsoCode = 'EUR';
        
        insert pallet;
        
        
    }
}