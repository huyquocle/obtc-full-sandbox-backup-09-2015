/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaymentPageControllerTest {

   public static Application_Config__c incomeURL;
    
     public static Application_Config__c SagePayVendorName;
      public static Application_Config__c incomeUserName;
    
    
    
    
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        incomeURL = CreateApplicationConfig('incomeURL', 'testurl');
        SagePayVendorName = CreateApplicationConfig('sagepayvendor', 'test');
        
        incomeUserName = CreateApplicationConfig('incomeUserName', 'test');

        PaymentPageController thiscon = new PaymentPageController();
        
        thiscon.autogetpaymentdetails();
        
        thiscon.paymentURL = 'testurl';
        
        
    }
    public static Application_Config__c CreateApplicationConfig(String appName, String val){
        Application_Config__c quantityRequestURL;
        quantityRequestURL = new Application_Config__c(Name=appName, Value__c = val);
        insert quantityRequestURL;
        return quantityRequestURL;
    }
}