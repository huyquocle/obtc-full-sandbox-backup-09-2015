@RestResource(urlMapping='/TrackingCode/*')
global class DocDataOrderTrackingCodeRestService {
    
    private static final string SUCCESS = 'Success';
    private static final string FAIL = 'Fail';
    private static final string INVALID_SO = 'Invalid Sales Order';
    private static final string INVALID_TRACKING_CODE = 'Invalid tracking code';
    private static final string UPDATE_FAIL = 'Update fail';
    
    // accept XML a list of Order Lists
    @HttpPost
    global static void TrackingCode (OrderList Order)
    {
        TriggerUtils.IsAllowUpdateDeleteSO = true;
        string status = SUCCESS;
        string message = '';
        //Logic to get the oderlists and update status
        List<PBSI__PBSI_Sales_Order__c> SOList =  [select id, name, PBSI__Status__c, PBSI__Tracking_Code__c                                                 
                                                   FROM PBSI__PBSI_Sales_Order__c 
                                                   WHERE name = :Order.OrderId.trim() and Sent_to_DC__c = true limit 1];
        
        
        
        if(SOList != null && SOList.size()> 0) {
            boolean isUpdateSO = false;
            //Update SO
            PBSI__PBSI_Sales_Order__c so = SOList[0];
            try{
                if(!String.isBlank(Order.TrackingCode)) {
                    so.PBSI__Tracking_Code__c = Order.TrackingCode;
                    update so;
                } else {
                    status = FAIL; 
                    message = INVALID_TRACKING_CODE;
                }  
            } catch(Exception e) {
                system.debug('Cause: ' + e.getCause() + ' Message: ' + e.getMessage());
                status = FAIL; 
                message = UPDATE_FAIL;
            }                      
        }
        else {
            status = FAIL; 
            message = INVALID_SO;
        }        
        string retXML = '<?xml version="1.0" encoding="UTF-8"?>';   
        retXML += '<response>';
        retXML += '<status>'+status+'</status>';
        retXML += '<message>' + message + '</message>';
        retXML += '</response>';
        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        RestContext.response.addHeader('Accept', 'application/xml');
        
        RestContext.response.responseBody = Blob.valueOf(retXML);
    }
    
    global class OrderList
    {
        global string OrderId {get;set;}
        global string TrackingCode {get;set;}    
        
    }
}