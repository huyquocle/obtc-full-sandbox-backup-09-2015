public class ODIXMLUtils {
	public static string XMLHeaderFooter(){
		string rtXML = '<?xml version="1.0" encoding="utf-8"?>\n';
		rtXML += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\n';
		rtXML += '\t<soap:Body>\n';
		rtXML += '\t</soap:Body>\n';
		rtXML += '</soap:Envelope>';
		return rtXML;
	}
	
	public static Dom.Document getBasedXML(){
		Dom.Document doc = new Dom.Document();
		doc.load(XMLHeaderFooter());
		return doc;
	}
	public static string testData(){
		string rtXML='<?xml version="1.0" encoding="utf-8"?>\n';
		rtXML += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\n';
		rtXML += '<soap:Body>\n';
		rtXML += '<getDispatchedOrdersResponse xmlns="http://warehouse.docdata.co.uk/">\n';
		rtXML += '<positionList xmlns="">\n';
		rtXML += '<orderRef>SO-0001</orderRef>\n';
		rtXML += '<Position>1</Position>\n';
		rtXML += '<status>SHIPPED</status>\n';
		rtXML += '<ForHoleOrder>True</ForHoleOrder>\n';
		rtXML += '<shippingDate>2015-2-22</shippingDate>\n';
		rtXML += '</positionList>\n';
		rtXML += '<positionList xmlns="">\n';
		rtXML += '<orderRef>SO-0002</orderRef>\n';
		rtXML += '<Position>1</Position>\n';
		rtXML += '<status>BACK_ORDER</status>\n';
		rtXML += '<ForHoleOrder>True</ForHoleOrder>\n';
		rtXML += '<shippingDate>2015-2-22</shippingDate>\n';
		rtXML += '</positionList>\n';
		rtXML += '</getDispatchedOrdersResponse>\n';
		rtXML += '</soap:Body>\n';
		rtXML += '</soap:Envelope>\n';
		return rtXML;
	
	}
}