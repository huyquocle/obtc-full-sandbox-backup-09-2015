public with sharing class PostNewOrder {

    public string OrderId { get; set; }
    public string Company { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string AddressLine1 { get; set; }
    public string AddressLine2 { get; set; }
    public string City { get; set; }
    public string Postcode { get; set; }
    public string Country { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Fax { get; set; }
    public boolean IsResidential { get; set; }
    public string OrderDate { get; set; }
    public string ShipmentMethod { get; set; }
    public List<OrderLine> OrderLines { get; set; }
    public Decimal FinalOrderTotal{set;get;}
    public Decimal FinalOrderTotalIncTax{set;get;}

    
    
    public class OrderLine
    {
        public string SKU { get; set; }
        public integer Quantity { get; set; }
        public Decimal SalePrice{set;get;}
    }
}