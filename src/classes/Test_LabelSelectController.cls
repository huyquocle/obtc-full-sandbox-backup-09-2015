@istest(seealldata=true)
public class Test_LabelSelectController {

    public static testmethod void Test() {
        PageReference pageRef = Page.LabelSelect;
        Test.setCurrentPage(pageRef);
        
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
        insert itemGroup;
    
        PBSI__PBSI_Location__c loc=new PBSI__PBSI_Location__c(Name='asfafasf');  
        insert loc;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c(Name='MON-LEWISHAM-PRELIM');
        item.PBSI__Default_Location__c  = loc.ID;
        item.PBSI__Item_Group__c        = itemGroup.ID;
        item.PBSI__description__c = 'description';
        item.Name = 'Test Name 1';
        item.Ascent4Prods__Do_Not_Sync__c = false;
        insert item;
        
        ApexPages.currentPage().getParameters().put('itd', item.Id);
        
        LabelSelectController controller = new LabelSelectController();
        controller.changeLabel();
    }
    
}