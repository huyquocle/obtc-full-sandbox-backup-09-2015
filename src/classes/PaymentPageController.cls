public with sharing class PaymentPageController {
    public string paymentURL {get; set;}

    public  PaymentPageController (){
    //paymentURL = Apexpages.currentPage().getParameters().get('payurl');
    
    //system.debug('####paymentURL ' + paymentURL);
    }
    
    
    
    public Pagereference autogetpaymentdetails(){
    
        system.debug('################# autogetpaymentdetails just started');
        
        paymentURL = string.valueOf(Application_Config__c.getInstance('incomeURL').Value__c);
        system.debug('################# paymentURL =' + paymentURL);
        //paymentURL += '?salesforceconnectionid=' + Userinfo.getSessionId() + '&salesforceconnectionurl=' +String.valueof(url.getSalesforceBaseUrl().toExternalForm()).replace('-api.salesforce.com','') + '/services/Soap/u/9.0/' + userinfo.getOrganizationId();
       
        paymentURL += '?salesforceconnectionid=' + Apexpages.currentPage().getParameters().get('salesforceconnectionid') + '&salesforceconnectionurl=' +Apexpages.currentPage().getParameters().get('salesforceconnectionurl');
        paymentURL += '&firstsfreferencename=' +  Apexpages.currentPage().getParameters().get('firstsfreferencename') ;  
        
        system.debug('################# paymentURL =' + paymentURL);
        paymentURL +=  '&softwareversion=3&salesforcecardpaymentrecordtype=Standard Card Payment&firstsfreferenceobject=Opportunity__c&firstsfreferenceobjectname=Opportunity&firstsfreferenceid=' +Apexpages.currentPage().getParameters().get('firstsfreferenceid');
        //system.debug('################# paymentURL =' + paymentURL);
        //paymentURL += '&secondsfreferenceobject=Account__c&secondsfreferenceid=' +  Apexpages.currentPage().getParameters().get('secondsfreferenceid') ;
        system.debug('################# paymentURL =' + paymentURL);
        paymentURL += '&billingaddress=' + Apexpages.currentPage().getParameters().get('billingaddress');
        paymentURL += '&billingpostcode=' + Apexpages.currentPage().getParameters().get('billingpostcode');
        paymentURL += '&billingcity=' + Apexpages.currentPage().getParameters().get('billingcity');
        paymentURL += '&billingcountrycode=' + Apexpages.currentPage().getParameters().get('billingcountrycode');
        paymentURL += '&billingstate=' + Apexpages.currentPage().getParameters().get('billingstate');
        
        
        paymentURL += '&billingemail=' + Apexpages.currentPage().getParameters().get('billingemail');
        paymentURL += '&billingfirstname=' + Apexpages.currentPage().getParameters().get('billingfirstname');
        paymentURL += '&billingsurname=' + Apexpages.currentPage().getParameters().get('billingsurname');
        paymentURL += '&deliveryaddress=' + Apexpages.currentPage().getParameters().get('billingaddress');
        paymentURL += '&deliverypostcode=' + Apexpages.currentPage().getParameters().get('billingpostcode');
        paymentURL += '&deliverycity=' + Apexpages.currentPage().getParameters().get('billingcity');
        paymentURL += '&deliverycountrycode=' + Apexpages.currentPage().getParameters().get('deliverycountrycode');    
        
        paymentURL += '&deliverystate=' + Apexpages.currentPage().getParameters().get('deliverystate');      
        paymentURL += '&deliveryemail=' + Apexpages.currentPage().getParameters().get('billingemail');
        paymentURL += '&deliveryfirstname=' + Apexpages.currentPage().getParameters().get('billingfirstname');
        paymentURL += '&deliverysurname=' + Apexpages.currentPage().getParameters().get('billingsurname');
        paymentURL += '&salecurrencycode=' +  Apexpages.currentPage().getParameters().get('salecurrencycode');
        paymentURL += '&saleamount=' +  Apexpages.currentPage().getParameters().get('saleamount');
        paymentURL += '&multicurrencymode=true';
        system.debug('################# incomeUserName =' + Apexpages.currentPage().getParameters().get('incomeusername'));
        paymentURL += '&incomeusername=' + Apexpages.currentPage().getParameters().get('incomeusername');
        paymentURL += '&sagepayvendor=' + Apexpages.currentPage().getParameters().get('sagepayvendor');
        paymentURL += '&paymentreason=Only+Payment&paymentdescription=' + Apexpages.currentPage().getParameters().get('paymentdescription');
        paymentURL += '&usebasket=false';
        paymentURL += '&rpenabled=false';

        system.debug('################# paymentURL =' + paymentURL);
    return null;
    }

}