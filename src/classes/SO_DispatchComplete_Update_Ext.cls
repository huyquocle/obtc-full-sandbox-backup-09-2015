public with sharing class SO_DispatchComplete_Update_Ext {
    list<PBSI__PBSI_Sales_Order__c> lstSO = new list<PBSI__PBSI_Sales_Order__c>();
    public SO_DispatchComplete_Update_Ext (ApexPages.StandardSetController cont){
        lstSO = [select id, PBSI__Stage__c from PBSI__PBSI_Sales_Order__c where id in: cont.getSelected()];
    }
    
    public PageReference updateSO(){
        
        for(PBSI__PBSI_Sales_Order__c pso: lstSO){
            pso.PBSI__Stage__c = 'Dispatch Complete';
            pso.Locked__c = false;
        }
        
        if(lstSO.size() > 0){
            update lstSO;
        }
        String returnUrl = ApexPages.currentPage().getParameters().get('retUrl');
        PageReference pageRef = new PageReference(returnUrl);
        
        return pageRef;
    }
}