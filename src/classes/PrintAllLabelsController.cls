public class PrintAllLabelsController {
    
    public List<PBSI__PBSI_Sales_Order_Line__c> allLabels{set;get;}
    public PrintAllLabelsController()
    {
        String soid = ApexPages.currentPage().getParameters().get('soid');
        if(soid != '')
        {
            allLabels = new List<PBSI__PBSI_Sales_Order_Line__c>();
            
            for(PBSI__PBSI_Sales_Order_Line__c sol: [SELECT ID,PBSI__ItemDescription__c,
                         PBSI__Item__r.Name,
                         PBSI__Sales_Order__r.Opp_ID__c,
                         PBSI__Quantity_Needed__c,
                         Label_Image_From_Item__c,
                         PBSI__Item__r.Bar_code_Image_url__c,
                         PBSI__Sales_Order__r.PBSI__Opportunity__r.Name,
                         
                         PBSI__Sales_Order__r.Customer_Order_Number__c,
                         PBSI__Sales_Order__r.PBSI__Customer__r.Name,                      
                         PBSI__Item__r.Barcode_Number_id__c
                         FROM  PBSI__PBSI_Sales_Order_Line__c WHERE 
                         PBSI__Sales_Order__c =:soid and PBSI__Item__r.Name !='SH000'
                        ]){
                            if(sol.PBSI__Quantity_Needed__c != null && sol.PBSI__Quantity_Needed__c >0){
                                for(integer i=0; i < sol.PBSI__Quantity_Needed__c; i++){
                                    allLabels.add(sol);
                                }
                            }
                        }
        }
    }
}