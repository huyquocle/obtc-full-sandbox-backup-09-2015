/*
*       Changes:
*           - <sonny.le@huntermacdonald.com> 
*				24/07/2015 Allow flow to invoke the Ascent MoveItem API call
*           		
*					moveItem(Id itemid, Id fromlotid, Id tolotid, Id tolocation, Decimal quantity, String serialnumbers) 
*
*/


global class flowAscentMoveItemCall {
    @InvocableMethod(label='Move Item')
    global static void doItemMovementAPI(List<itemMovementRequest> requests) {
        system.debug('***** requests: ' + requests);
        for (itemMovementRequest request : requests) {
            TriggerUtils.movementLineAjustmentReason = 'Booked in at DocData';
            PBSI.AscentAPI.moveItem(request.itemid, request.fromlotid, null, request.tolocation, request.quantity, null);
            TriggerUtils.movementLineAjustmentReason = '';
        } 
    }

    global class itemMovementRequest {
        @InvocableVariable(required=true)
        public ID itemId;

        @InvocableVariable(required=true)
        public ID fromlotid;

        @InvocableVariable(required=false)
        public ID tolotid;

        @InvocableVariable(required=true)
        public ID tolocation;

        @InvocableVariable(required=true)
        public Decimal quantity;
    }
	
    /*global Process.PluginResult invoke(Process.PluginRequest request){
		// Input parameters from the flow.
        String itemId = (String) request.inputParameters.get('itemID');
        String fromlotid = (String) request.inputParameters.get('fromlotid');
        String tolotid = (String) request.inputParameters.get('tolotid');
        String tolocation = (String) request.inputParameters.get('tolocation');
        Decimal quantity = (Decimal) request.inputParameters.get('quantity');


        system.debug('***** itemId ' + itemID );
        system.debug('***** fromlotid ' + fromlotid );
        system.debug('***** tolotid ' + tolotid );
        system.debug('***** tolocation ' + tolocation );
        system.debug('***** quantity ' + quantity );

        // Call AscentAPI to moveItem
        //try {
            //PBSI.AscentAPI.moveItem(itemId, fromLotId, null, toLocation, quantity, null);
            PBSI.AscentAPI.MoveItem('a3T200000008XTN', 'a3KK0000000HlHt', 'a3KK0000000HkME', 'a3UK00000026Yuv', 4.0, '');
        //} catch(Exception ex) {         	
        //	throw new AscentPluginException('Error calling AscentAPI MoveItem: ' + ex);
        //}        

        Map<String,Object> result = new Map<String,Object>();
        result.put('accountId', account.Id);
        return new Process.PluginResult(result);
	}


    global Process.PluginDescribeResult describe() {
        // Set up plugin metadata
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.description = 'The Flow Plug-in calls MoveItem API from Ascent';
        result.tag = 'AscentAPI MoveItem';
        
     	result.inputParameters = new List <Process.PluginDescribeResult.InputParameter> {
            // All params are mandatory
            new Process.PluginDescribeResult.InputParameter('itemID', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('fromlotid', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('tolotid', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('tolocation', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('quantity', Process.PluginDescribeResult.ParameterType.DECIMAL, true)                                          
    	};

        return result;
    }
        

	// Utility exception class
    class AscentPluginException extends Exception {}
    */
}