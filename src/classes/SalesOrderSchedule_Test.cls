@isTest()
public class SalesOrderSchedule_Test {
    static testmethod void Test() {
        
        Account  account = new Account ();
        account.Name = 'test account';
		insert account; 
        
        Opportunity opp = new Opportunity ();
        opp.Name = 'test';
        opp.Account = account;
        opp.CloseDate = system.Date.today();
        opp.StageName = 'Prospecting';
        opp.Shipping_and_Handling__c = 100;
        opp.VAT_Code__c = '12345678';
        insert opp;
        
        PBSI__PBSI_Sales_Order__c salesOrder = new PBSI__PBSI_Sales_Order__c();
        salesOrder.PBSI__Stage__c = 'Packed';
        salesOrder.Payment_Required__c = false;
        salesOrder.Hold_Until__c = DateTime.Now().Date().addDays(-1);
        salesOrder.PBSI__Opportunity__c = opp.id;
        insert salesOrder;
        
        SalesOrderUpdateBatch b = new SalesOrderUpdateBatch(); 
        database.executebatch(b,2);
        
        SalesOrderUpdateSchedule she = new SalesOrderUpdateSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Merge Job', sch, she);
    }
}