/*
*       Changes:
*           - <sonny.le@softqware.com> 24/07/2015 ODI-39: Added to, from location to Stock Adjust.
*
*
*/


@RestResource(urlMapping='/StockAdjust/*')
global class StockAdjustRestService {

    @HttpPost
    global static void adjust(ItemRequest Item) {
        
        string status = 'Success';
        string message = '';
        string UNDOCUMENTED_RETURN = 'Undocumented Return';
        string DAMAGED_TRANSIT = 'Damaged in Transit';
        string DAMAGED_DOCDATA = 'Damaged at DocData';
        string FOUND = 'Found';
        string THEFT = 'Theft';
        ID fromLotId = null;
        ID toLotId = null;
        ID toLocation = null;
        double adjustQuantity, docdataQuantity, salesforceQuantity;    
        
        //get item
        //List<PBSI__PBSI_Item__c> lstItem = [select id, name from PBSI__PBSI_Item__c where name = :Item.SKU limit 1];
        List<PBSI__Movement_Line__c> lstItem = [select PBSI__Item__r.Id ,PBSI__Item__r.Name,Name 
                                                FROM  PBSI__Movement_Line__c where PBSI__Item__r.Name =:Item.SKU 
                                                AND PBSI__Movement_Journal__r.PBSI__Type__c ='Transfer Posting Location to Location' 
                                                AND PBSI__To_Location__r.Name ='DocData' limit 1];

        if(lstItem != null && !lstItem.isEmpty()) {
            //get lot and parent lot
            List<PBSI__Lot__c> lot = [select id, name, PBSI__Total_Quantity__c,PBSI__Location__c, PBSI__Parent_Lot__r.id from PBSI__Lot__c where Name = 'DocData' and PBSI__Item__r.name = :lstItem[0].PBSI__Item__r.Name limit 1];

            fromLotId = lot[0].Id;  
            salesforceQuantity = lot[0].PBSI__Total_Quantity__c;
            docdataQuantity = Item.Quantity;
            TriggerUtils.movementLineAjustmentReason = item.ReasonCode;

            if (item.ReasonCode != null){
                if (item.ReasonCode == UNDOCUMENTED_RETURN) {
                    toLocation = [select Id from PBSI__PBSI_Location__c where Name = 'DocData Returns' LIMIT 1].Id;   // NOTE: location must be exist.  
                    adjustQuantity = salesforceQuantity - docdataQuantity;
                } else if (item.ReasonCode == DAMAGED_TRANSIT || item.ReasonCode == DAMAGED_DOCDATA) {
                    toLocation = [select Id from PBSI__PBSI_Location__c where Name = 'DocData Damaged' LIMIT 1].Id;   // NOTE: location must be exist.   
                    adjustQuantity = salesforceQuantity - docdataQuantity;
                } else if (item.ReasonCode == FOUND) {
                    fromLotId = null;
                    toLotId = lot[0].Id;
                    adjustQuantity = docdataQuantity - salesforceQuantity;
                } else if (item.ReasonCode == THEFT) {
                    fromLotId = lot[0].Id;
                    toLotId = null;
                    adjustQuantity = salesforceQuantity - docdataQuantity;
                }
            }

            // Move Item API Call     
            //moveItem(Id itemid, Id fromlotid, Id tolotid, Id tolocation, Decimal quantity, String serialnumbers) 
            try {
                PBSI.AscentAPI.moveItem(lstItem[0].PBSI__Item__r.Id, fromLotId, toLotId, toLocation, adjustQuantity, null);
            } catch(Exception ex) {
                
            }
             

            TriggerUtils.movementLineAjustmentReason ='';            
        } else {
            status = 'Fail';
            message = 'Invalid Item';
        }
        
        string retXML = '<?xml version="1.0" encoding="UTF-8"?>';   
        retXML += '<response>';
        retXML += '<status>'+status+'</status>';
        retXML += '<message>' + message + '</message>';
        retXML += '</response>';
        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        RestContext.response.addHeader('Accept', 'application/xml');

        RestContext.response.responseBody = Blob.valueOf(retXML);
    }    
    
    global class ItemRequest {
        global String SKU{get;set;}
        global integer Quantity{get;set;}        
        global String ReasonCode{get;set;}
    }
}