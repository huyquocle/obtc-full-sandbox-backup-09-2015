public class PrintAllPWOLabelsContrller {

    public List<PBSI__Production_Work_Order__c> allLabels{set;get;}
    public PrintAllPWOLabelsContrller(ApexPages.StandardController stdController) 
    {

        PBSI__Production_Work_Order__c item = (PBSI__Production_Work_Order__c)stdController.getRecord();
        
        if(item != null)        
        {
             allLabels = new List<PBSI__Production_Work_Order__c>();
             for(PBSI__Production_Work_Order__c items:[SELECT ID,Name,
                         PBSI__Item__r.Name,
                         PBSI__Quantity_Left__c,
                         PBSI__Quantity_Built__c,
                         PBSI__Item_Description__c,
                         Barcode_Number_id__c,
                         PBSI__Quantity__c,
                         PBSI__Item__r.PBSI__Photo_URL__c
                         from PBSI__Production_Work_Order__c where id =: item.id])
             {
                  if(items.PBSI__Quantity__c!= null && items.PBSI__Quantity__c>0){
                                for(integer i=0; i < items.PBSI__Quantity__c; i++){
                                    allLabels.add(items);
                                }
                            }
              
                 
             }
        }

    }

}