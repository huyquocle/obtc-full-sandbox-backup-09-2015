global class SalesOrderUpdateBatch implements Database.Batchable<sObject> {
    
    global SalesOrderUpdateBatch() {
        
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT ID, PBSI__Stage__c, Payment_Required__c, Hold_Until__c FROM PBSI__PBSI_Sales_Order__c WHERE PBSI__Status__c != \'Cancelled\' AND PBSI__Stage__c = \'Packed\'');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<PBSI__PBSI_Sales_Order__c> salesOrders = (list<PBSI__PBSI_Sales_Order__c>)scope;
        list<PBSI__PBSI_Sales_Order__c> insertSalesOrders = new list<PBSI__PBSI_Sales_Order__c>();
        for(PBSI__PBSI_Sales_Order__c so : salesOrders){
            if(so.Hold_Until__c == null)
            	{
	                if (!so.Payment_Required__c) {
	                    so.PBSI__Stage__c = 'Ready for Dispatch';
	                    insertSalesOrders.add(so);
	                }
            	}else if(so.Hold_Until__c <= System.Today()){    		
	                if (!so.Payment_Required__c) {
	                    so.PBSI__Stage__c = 'Ready for Dispatch';
	                    insertSalesOrders.add(so);
	                }
            	}
        }
        update insertSalesOrders;
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
}