@istest
public class RMATriggersTest {
static testMethod void Test()
    {
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        Case aCase = new Case(OwnerId = testGroup.Id);
        insert aCase;
    }
}