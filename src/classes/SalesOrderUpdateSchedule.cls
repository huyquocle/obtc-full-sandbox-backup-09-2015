global class SalesOrderUpdateSchedule implements Schedulable{
   global void execute(SchedulableContext sc) {
      SalesOrderUpdateBatch b = new SalesOrderUpdateBatch(); 
      database.executebatch(b,10);
   }
}