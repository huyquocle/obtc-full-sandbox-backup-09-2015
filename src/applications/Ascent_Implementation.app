<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>App containing objects relating to Ascent Implementation. Set up by Rob Ralston of CRMSOS October 2014</description>
    <label>Ascent Implementation</label>
    <logo>Ascent_Documents/Ascent_Logo.jpg</logo>
    <tab>User_Story__c</tab>
    <tab>Income_Card_Payment__c</tab>
    <tab>AccountAddress__c</tab>
    <tab>Category__c</tab>
    <tab>Country__c</tab>
    <tab>Project_Contact__c</tab>
    <tab>Feedback__c</tab>
</CustomApplication>
